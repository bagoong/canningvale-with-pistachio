/**
  * @name : pistachio-connect handlebars helpers
  * @desc : This is used to fix gaps on inbound requests from external services
  * @desc : Usually invoked by templates that need adjustments done inline
  * @desc Date Created : 2017-01-01
  * @desc Created by David Imrie
  * @desc Version 1.0.2
  * Adjusted
    escapeChars - added toString() to clear it as a string
  * Added the following Helpers
      countryToCode
	  getState - extracts US,AU,MEX states to ISO 2 code standards for NS
  * Deprecated
    stateToCode - original Australian States List
*/
// jsonSafe - cleans a string to make it safe for transmission
Handlebars.registerHelper('jsonSafe', function(dataIn) {
		var returnString = JSON.stringify(dataIn);
return returnString;
});
// end function

Handlebars.registerHelper('stripLineBreaks', function (text) {
  return (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + " " + '$2');
});
// end function

// neto phone number fixer
Handlebars.registerHelper('netoPhoneNumber', function(string,idx) {
var returnString = '';
		if(string)
		{
			var newString = string.split('or');
				if(newString.length === 2)
				{
				returnString = newString[(idx-1)];
				}
				else
				{
				returnString = newString;
				}
		}
return returnString;
});
// end function


Handlebars.registerHelper('dateTimeToDate', function(string) {
var returnString = '';
if(string)
{
	var dateString = string;
	var splitString = dateString.split(' ');
	var returnDate = splitString[0];
	var newDate = returnDate.split('-');
	if(newDate.length)
	{
	returnString = newDate[2] + '/' + newDate[1] + '/' + newDate[0];
	}
}
return returnString;
});

// fixes control characters in a string from external providers
Handlebars.registerHelper('escapeChars', function(string) {
  var returnString = '';
  if(string)
    {
      var dataIn = string;
          dataIn = dataIn.toString();
      var returnString = dataIn.replace("'","\'").replace('"','\"');
    }
  return returnString;
});

// multiply helper - default unit is 6
Handlebars.registerHelper('multiply', function(rate) {
	var quantity = 6;
    var returnString = parseInt(rate) * quantity;
return returnString;
});

// AU GST HELPERS
Handlebars.registerHelper('GSTAU', function(rate) {
	var tax = 11;
	var gst  = rate / tax;
	var amount = gst.toFixed(4);
        var returnString = amount;
return returnString;
});
// end helper 

Handlebars.registerHelper('addGSTAU', function(rate) {
if(!isNaN(rate))
{
var gst   = 1.1;
var inc   = rate * gst;
return inc.toFixed(4);
}
else
{
return 0.00;
}
});
// end helper 

Handlebars.registerHelper('exGSTAU', function(rate) {
	var tax = 11;
	var gst  = rate / tax;
	var amount = gst;		// .toFixed(6);
    var returnString = rate - amount;
return returnString;
});
// end helper

// NZ Helpers 
Handlebars.registerHelper('addGSTNZ', function(rate) {
  var amount  = rate * 1.15;
      amount =  parseFloat(amount);
      return amount.toFixed(4);
});
// end helper

Handlebars.registerHelper('exGSTNZ', function(rate) {
  var gst  = rate * 3 / 23;
  var amount = rate - gst; 
  return amount.toFixed(4);	
});
// end helper 


Handlebars.registerHelper('GSTNZ', function(rate) {
  var amount  = rate * 3 /23;
      amount =  parseFloat(amount);
      return amount.toFixed(4);
});
// end helper

Handlebars.registerHelper('if_eq', function(a, b, opts) {
    if (a == b) {
        return opts.fn(this);
    } else {
        return opts.inverse(this);
    }
});
// end function

Handlebars.registerHelper('if_not_eq', function(a, b, opts) {
    if (a !== b) {
        return opts.fn(this);
    } else {
        return opts.inverse(this);
    }
});
// end function

Handlebars.registerHelper('iflessthan', function(value1, value2, options) {
  if(value1 < value2) {
    return options.fn(this);
  }
  return options.inverse(this);
});


Handlebars.registerHelper('ifgreaterthan', function(value1, value2, options) {
  if(value1 > value2) {
    return options.fn(this);
  }
  return options.inverse(this);
});

Handlebars.registerHelper('pluralize', function(number, single, plural) {
  if (number === 1) { return single; }
  else { return plural; }
});
// end function


Handlebars.registerHelper('getState', function(country, state) {
	state = state.toString();
	var states = {};
	var returnString = state;

	switch(country)
	{
  case 'US':
  case 'United States':
 				states = {
								 'Alabama':'AL',
								 'Alaska':'AK',
								 'Arizona':'AZ',
								 'Arkansas':'AR',
								 'California':'CA',
								 'Colorado':'CO',
								 'Connecticut':'CT',
								 'Delaware':'DE',
								 'District Of Columbia':'DC',
								 'Florida':'FL',
								 'Georgia':'GA',
								 'Hawaii':'HI',
								 'Idaho':'ID',
								 'Illinois':'IL',
								 'Indiana':'IN',
								 'Iowa':'IA',
								 'Kansas':'KS',
								 'Kentucky':'KY',
								 'Louisiana':'LA',
								 'Maine':'ME',
								 'Maryland':'MD',
								 'Massachusetts':'MA',
								 'Michigan':'MI',
								 'Minnesota':'MN',
								 'Mississippi':'MS',
								 'Missouri':'MO',
								 'Montana':'MT',
								 'Nebraska':'NE',
								 'Nevada':'NV',
								 'New Hampshire':'NH',
								 'New Jersey':'NJ',
								 'New Mexico':'NM',
								 'New York':'NY',
								 'North Carolina':'NC',
								 'North Dakota':'ND',
								 'Ohio':'OH',
								 'Oklahoma':'OK',
								 'Oregon':'OR',
								 'Pennsylvania':'PA',
								 'Rhode Island':'RI',
								 'South Carolina':'SC',
								 'South Dakota':'SD',
								 'Tennessee':'TN',
								 'Texas':'TX',
								 'Utah':'UT',
								 'Vermont':'VT',
								 'Virginia':'VA',
								 'Washington':'WA',
								 'West Virginia':'WV',
								 'Wisconsin':'WI',
								 'Wyoming':'WY'
							 };
							 returnString = states[state];
  break;
  
  case 'AU':
  case 'Australia':
				states = {
								'Australian Capital Territory' : 'ACT',
								'New South Wales' : 'NSW',
								'Northern Territory' : 'NT',
								'Queensland' : 'QLD',
								'South Australia' : 'SA',
								'Tasmania' : 'TAS',
								'Victoria' : 'VIC',
								'Western Australia' : 'WA'
							};
							returnString = states[state];
  break;

  case 'Mexico':
				states = {
									"Distrito Federal":"DIF",
									"Aguascalientes":"AGS",
									"Baja California":"BCN",
									"Baja California Sur":"BCS",
									"Campeche":"CAM",
									"Chiapas":"CHP",
									"Chihuahua":"CHI",
									"Coahuila":"COA",
									"Colima":"COL",
									"Durango":"DUR",
									"Guanajuato":"GTO",
									"Guerrero":"GRO",
									"Hidalgo":"HGO",
									"Jalisco":"JAL",
									"M&eacute;xico":"MEX",
									"Michoac&aacute;n":"MIC",
									"Morelos":"MOR",
									"Nayarit":"NAY",
									"Nuevo Le&oacute;n":"NLE",
									"Oaxaca":"OAX",
									"Puebla":"PUE",
									"Quer&eacute;taro":"QRO",
									"Quintana Roo":"ROO",
									"San Luis Potos&iacute;":"SLP",
									"Sinaloa":"SIN",
									"Sonora":"SON",
									"Tabasco":"TAB",
									"Tamaulipas":"TAM",
									"Tlaxcala":"TLX",
									"Veracruz":"VER",
									"Yucat&aacute;n":"YUC",
									"Zacatecas":"ZAC"							
								};
			returnString = states[state];
  break;

	default:
			returnString = returnString;
	break;
}
return returnString;
});
// end function 




// for legacy support
Handlebars.registerHelper('stateToCode', function(dataIn) {
var returnString = '';
  if(dataIn)
    {
    dataIn = dataIn.toString();
	  var states = {
                'Australian Capital Territory' : 'ACT',
                'New South Wales' : 'NSW',
                'Northern Territory' : 'NT',
                'Queensland' : 'QLD',
                'South Australia' : 'SA',
                'Tasmania' : 'TAS',
                'Victoria' : 'VIC',
                'Western Australia' : 'WA'
							};
							returnString = states[dataIn];
      }
return returnString;
});
// end function

Handlebars.registerHelper('countryToCode', function(dataIn) {
var returnString = '';
  if(dataIn)
    {
      dataIn = dataIn.toString();
         var countries = {
                    "Afghanistan":"AF",
                    "Åland Islands":"AX",
                    "Albania":"AL",
                    "Algeria":"DZ",
                    "American Samoa":"AS",
                    "Andorra":"AD",
                    "Angola":"AO",
                    "Anguilla":"AI",
                    "Antarctica":"AQ",
                    "Antigua and Barbuda":"AG",
                    "Argentina":"AR",
                    "Armenia":"AM",
                    "Aruba":"AW",
                    "Australia":"AU",
                    "Austria":"AT",
                    "Azerbaijan":"AZ",
                    "Bahamas":"BS",
                    "Bahrain":"BH",
                    "Bangladesh":"BD",
                    "Barbados":"BB",
                    "Belarus":"BY",
                    "Belgium":"BE",
                    "Belize":"BZ",
                    "Benin":"BJ",
                    "Bermuda":"BM",
                    "Bhutan":"BT",
                    "Bolivia, Plurinational State of":"BO",
                    "Bonaire, Sint Eustatius and Saba":"BQ",
                    "Bosnia and Herzegovina":"BA",
                    "Botswana":"BW",
                    "Bouvet Island":"BV",
                    "Brazil":"BR",
                    "British Indian Ocean Territory":"IO",
                    "Brunei Darussalam":"BN",
                    "Bulgaria":"BG",
                    "Burkina Faso":"BF",
                    "Burundi":"BI",
                    "Cambodia":"KH",
                    "Cameroon":"CM",
                    "Canada":"CA",
                    "Cape Verde":"CV",
                    "Cayman Islands":"KY",
                    "Central African Republic":"CF",
                    "Chad":"TD",
                    "Chile":"CL",
                    "China":"CN",
                    "Christmas Island":"CX",
                    "Cocos (Keeling) Islands":"CC",
                    "Colombia":"CO",
                    "Comoros":"KM",
                    "Congo":"CG",
                    "Congo, the Democratic Republic of the":"CD",
                    "Cook Islands":"CK",
                    "Costa Rica":"CR",
                    "Côte d'Ivoire":"CI",
                    "Croatia":"HR",
                    "Cuba":"CU",
                    "Curaçao":"CW",
                    "Cyprus":"CY",
                    "Czech Republic":"CZ",
                    "Denmark":"DK",
                    "Djibouti":"DJ",
                    "Dominica":"DM",
                    "Dominican Republic":"DO",
                    "Ecuador":"EC",
                    "Egypt":"EG",
                    "El Salvador":"SV",
                    "Equatorial Guinea":"GQ",
                    "Eritrea":"ER",
                    "Estonia":"EE",
                    "Ethiopia":"ET",
                    "Falkland Islands (Malvinas)":"FK",
                    "Faroe Islands":"FO",
                    "Fiji":"FJ",
                    "Finland":"FI",
                    "France":"FR",
                    "French Guiana":"GF",
                    "French Polynesia":"PF",
                    "French Southern Territories":"TF",
                    "Gabon":"GA",
                    "Gambia":"GM",
                    "Georgia":"GE",
                    "Germany":"DE",
                    "Ghana":"GH",
                    "Gibraltar":"GI",
                    "Greece":"GR",
                    "Greenland":"GL",
                    "Grenada":"GD",
                    "Guadeloupe":"GP",
                    "Guam":"GU",
                    "Guatemala":"GT",
                    "Guernsey":"GG",
                    "Guinea":"GN",
                    "Guinea-Bissau":"GW",
                    "Guyana":"GY",
                    "Haiti":"HT",
                    "Heard Island and McDonald Islands":"HM",
                    "Holy See (Vatican City State)":"VA",
                    "Honduras":"HN",
                    "Hong Kong":"HK",
                    "Hungary":"HU",
                    "Iceland":"IS",
                    "India":"IN",
                    "Indonesia":"ID",
                    "Iran, Islamic Republic of":"IR",
                    "Iraq":"IQ",
                    "Ireland":"IE",
                    "Isle of Man":"IM",
                    "Israel":"IL",
                    "Italy":"IT",
                    "Jamaica":"JM",
                    "Japan":"JP",
                    "Jersey":"JE",
                    "Jordan":"JO",
                    "Kazakhstan":"KZ",
                    "Kenya":"KE",
                    "Kiribati":"KI",
                    "Korea, Democratic People's Republic of":"KP",
                    "Korea, Republic of":"KR",
                    "Kuwait":"KW",
                    "Kyrgyzstan":"KG",
                    "Lao People's Democratic Republic":"LA",
                    "Latvia":"LV",
                    "Lebanon":"LB",
                    "Lesotho":"LS",
                    "Liberia":"LR",
                    "Libya":"LY",
                    "Liechtenstein":"LI",
                    "Lithuania":"LT",
                    "Luxembourg":"LU",
                    "Macao":"MO",
                    "Macedonia, the former Yugoslav Republic of":"MK",
                    "Madagascar":"MG",
                    "Malawi":"MW",
                    "Malaysia":"MY",
                    "Maldives":"MV",
                    "Mali":"ML",
                    "Malta":"MT",
                    "Marshall Islands":"MH",
                    "Martinique":"MQ",
                    "Mauritania":"MR",
                    "Mauritius":"MU",
                    "Mayotte":"YT",
                    "Mexico":"MX",
                    "Micronesia, Federated States of":"FM",
                    "Moldova, Republic of":"MD",
                    "Monaco":"MC",
                    "Mongolia":"MN",
                    "Montenegro":"ME",
                    "Montserrat":"MS",
                    "Morocco":"MA",
                    "Mozambique":"MZ",
                    "Myanmar":"MM",
                    "Namibia":"NA",
                    "Nauru":"NR",
                    "Nepal":"NP",
                    "Netherlands":"NL",
                    "New Caledonia":"NC",
                    "New Zealand":"NZ",
                    "Nicaragua":"NI",
                    "Niger":"NE",
                    "Nigeria":"NG",
                    "Niue":"NU",
                    "Norfolk Island":"NF",
                    "Northern Mariana Islands":"MP",
                    "Norway":"NO",
                    "Oman":"OM",
                    "Pakistan":"PK",
                    "Palau":"PW",
                    "Palestinian Territory, Occupied":"PS",
                    "Panama":"PA",
                    "Papua New Guinea":"PG",
                    "Paraguay":"PY",
                    "Peru":"PE",
                    "Philippines":"PH",
                    "Pitcairn":"PN",
                    "Poland":"PL",
                    "Portugal":"PT",
                    "Puerto Rico":"PR",
                    "Qatar":"QA",
                    "Réunion":"RE",
                    "Romania":"RO",
                    "Russian Federation":"RU",
                    "Rwanda":"RW",
                    "Saint Barthélemy":"BL",
                    "Saint Helena, Ascension and Tristan da Cunha":"SH",
                    "Saint Kitts and Nevis":"KN",
                    "Saint Lucia":"LC",
                    "Saint Martin (French part)":"MF",
                    "Saint Pierre and Miquelon":"PM",
                    "Saint Vincent and the Grenadines":"VC",
                    "Samoa":"WS",
                    "San Marino":"SM",
                    "Sao Tome and Principe":"ST",
                    "Saudi Arabia":"SA",
                    "Senegal":"SN",
                    "Serbia":"RS",
                    "Seychelles":"SC",
                    "Sierra Leone":"SL",
                    "Singapore":"SG",
                    "Sint Maarten (Dutch part)":"SX",
                    "Slovakia":"SK",
                    "Slovenia":"SI",
                    "Solomon Islands":"SB",
                    "Somalia":"SO",
                    "South Africa":"ZA",
                    "South Georgia and the South Sandwich Islands":"GS",
                    "South Sudan":"SS",
                    "Spain":"ES",
                    "Sri Lanka":"LK",
                    "Sudan":"SD",
                    "Suriname":"SR",
                    "Svalbard and Jan Mayen":"SJ",
                    "Swaziland":"SZ",
                    "Sweden":"SE",
                    "Switzerland":"CH",
                    "Syrian Arab Republic":"SY",
                    "Taiwan, Province of China":"TW",
                    "Tajikistan":"TJ",
                    "Tanzania, United Republic of":"TZ",
                    "Thailand":"TH",
                    "Timor-Leste":"TL",
                    "Togo":"TG",
                    "Tokelau":"TK",
                    "Tonga":"TO",
                    "Trinidad and Tobago":"TT",
                    "Tunisia":"TN",
                    "Turkey":"TR",
                    "Turkmenistan":"TM",
                    "Turks and Caicos Islands":"TC",
                    "Tuvalu":"TV",
                    "Uganda":"UG",
                    "Ukraine":"UA",
                    "United Arab Emirates":"AE",
                    "United Kingdom":"GB",
                    "United States":"US",
                    "United States Minor Outlying Islands":"UM",
                    "Uruguay":"UY",
                    "Uzbekistan":"UZ",
                    "Vanuatu":"VU",
                    "Venezuela, Bolivarian Republic of":"VE",
                    "Viet Nam":"VN",
                    "Virgin Islands, British":"VG",
                    "Virgin Islands, U.S.":"VI",
                    "Wallis and Futuna":"WF",
                    "Western Sahara":"EH",
                    "Yemen":"YE",
                    "Zambia":"ZM",
                    "Zimbabwe":"ZW"
                }
          returnString = countries[dataIn];
      }
return returnString;
});
// end function

// returns true or false for matrix shipping values 
Handlebars.registerHelper('if_magentoMatrixShipping', function(shippingName) {
		var str = shippingName;
		var returnString = false;
		var n = str.indexOf("matrixrate_matrixrate");
		if(n >= 0)
		{
			return returnString;
		}
});
// end function

//trims a string that is processed by handlebars
Handlebars.registerHelper('trimstring', function(string) {
  var str = string;
  var returnString = str.trim();
  return returnString;
});
// end function

Handlebars.registerHelper('singleline', function (text) {
  return (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + " " + '$2');
});
// end function

Handlebars.registerHelper('nl2br', function (text) {
  return (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + "<br/>" + '$2');
});
// end function 

Handlebars.registerHelper('dump', function(data) {
 var returnString = JSON.stringify(data);
return returnString;
});



Handlebars.registerHelper('moneyFormat', function(rate) {
  if(! isNaN(rate))
	{
    var amount = rate;
    var amount = amount.toFixed(8);
    var returnString = amount;
    }
return returnString;
});
// end helper


// if a string value is null, it will return zero 0 in its place
Handlebars.registerHelper('defaultToZero', function(string) {
  var returnString = '';
  if(! isNaN(string))
    {
      var dataIn = string;
          dataIn = dataIn.toString();
          returnString = dataIn;
    }
	else
	{
	returnString = 0;
	}
  return returnString;
});


Handlebars.registerHelper('minusPercentage', function(number,percent) {

var returnString = 0;
if(! isNaN(number) && ! isNaN(percent))
	{
  var amount = number * percent / 100;
	var returnString = number - amount;
	returnString = Math.ceil(returnString);
	}
	return returnString;
});
// end helper


Handlebars.registerHelper('plusPercentage', function(number,percent) {

var returnString = 0;
if(! isNaN(number) && ! isNaN(percent))
	{
  var amount = number * percent / 100;
	var returnString = number + amount;
	returnString = Math.ceil(returnString);
	}
	return returnString;

});
// end helper

Handlebars.registerHelper('lastWord', function(words) {
  var returnString = '';
  var n = words.split(":");
    returnString = n[n.length - 1];
return returnString;
});

Handlebars.registerHelper('exVAT', function(rate) {
	var tax = 1.20;
	var gst  = rate / tax;
	var amount = rate - gst;
    var returnString = rate - amount;
return returnString.toFixed(2);
});
// end helper

Handlebars.registerHelper('divideUnitOfMeasure', function(value, uom) {
var returnString = 0.00;
if(! isNaN(value) )
{
switch(uom)
 {
   case 'lb':
   returnString = value * 0.4535;
   break;

   case 'g':
   returnString = value / 1000;
   break;

   case 'kg':
   returnString = value / 1;
   break;

   case 'oz':
   returnString = value * 0.02834;
   break;
 }
}
return returnString;
});
// end function

Handlebars.registerHelper('listFirst', function(string,delimiter) {
var returnString = '';
		if(string)
		{
			string = string.toString();
			var newString = string.split(delimiter);
				if(newString.length > 0)
				{
		 			 returnString = newString[0]
                     returnString = returnString.trim();
				}
				else
				{
					returnString = newString;
	                returnString = returnString.trim();
				}
		}
return returnString.trim();
});
// end function

Handlebars.registerHelper('listLast', function(string,delimiter) {
var returnString = '';
		if(string)
		{
			string = string.toString();
			var newString = string.split(delimiter);
				if(newString.length > 0)
				{
					returnString = newString[newString.length-1];
    	            returnString = returnString.trim();
				}
				else
				{
					returnString = newString;
                  	returnString = returnString.trim();
				}
		}
return returnString.trim();
});
// end function
