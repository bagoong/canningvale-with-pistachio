/**
  * @name : pistachio-connect-neto-functions.js
  * @desc : This library handles the requests specifically for the Neto library
  * @desc
  * @desc Date Created : 2017-
  * @desc Created by David Imrie
  * @desc Version 1.0
*/



function parseNetoHeaders(request)
{
	// @desc -this parses a request object for the webservice request for Neto.
	//var requestData = request.getParameter('content');
  	var netoRequest 	= {};
  	var requestData = request.getBody();
	var requestData = requestData.replace(/ns:Event/g, 'ns_Event');
  	var netoObj 			= xmlToJson(nlapiStringToXML(requestData));
	if(requestData)
			{
				var xmlString = nlapiStringToXML(requestData);
					// prepare an object for use to handle the Neto Object
					netoRequest.eventID				= netoObj.ns_Event.EventID;
					netoRequest.eventType 			= netoObj.ns_Event.EventType;
					netoRequest.OrderID 			= netoObj.ns_Event.Order.OrderID;
					netoRequest.OrderStatus   		= netoObj.ns_Event.Order.OrderStatus;
              		// end handling the Neto Request
					return netoRequest;
			}
}
// end function

function getNetoOrderPacket(netoOrderID)
{
var netoString = '{"Filter":{"OrderID":[""],"OutputSelector":["ID","ShippingOption","DeliveryInstruction","RelatedOrderID","Username","Email","ShipAddress","BillAddress","PurchaseOrderNumber","SalesPerson","CustomerRef1","CustomerRef2","CustomerRef3","CustomerRef4","CustomerRef5","CustomerRef6","CustomerRef7","CustomerRef8","CustomerRef9","CustomerRef10","SalesChannel","GrandTotal","TaxInclusive","OrderTax","SurchargeTotal","SurchargeTaxable","ProductSubtotal","ShippingTotal","ShippingTax","ClientIPAddress","CouponCode","CouponDiscount","ShippingDiscount","OrderType","OnHoldType","OrderStatus","OrderPayment","OrderPayment.PaymentType","OrderPayment.DatePaid","DateUpdated","DatePlaced","DateRequired","DateInvoiced","DatePaid","DateCompleted","OrderLine","OrderLine.ProductName","OrderLine.ItemNotes","OrderLine.SerialNumber","OrderLine.PickQuantity","OrderLine.BackorderQuantity","OrderLine.UnitPrice","OrderLine.Tax","OrderLine.TaxCode","OrderLine.WarehouseID","OrderLine.WarehouseName","OrderLine.WarehouseReference","OrderLine.Quantity","OrderLine.PercentDiscount","OrderLine.ProductDiscount","OrderLine.CouponDiscount","OrderLine.CostPrice","OrderLine.ShippingMethod","OrderLine.ShippingTracking","OrderLine.Weight","OrderLine.Cubic","OrderLine.Extra","OrderLine.ExtraOptions","OrderLine.BinLoc","OrderLine.QuantityShipped","ShippingSignature","RealtimeConfirmation","InternalOrderNotes","OrderLine.eBay.eBayUsername","OrderLine.eBay.eBayStoreName","OrderLine.eBay.eBayTransactionID","OrderLine.eBay.eBayAuctionID","OrderLine.eBay.ListingType","OrderLine.eBay.DateCreated","CompleteStatus","OrderLine.eBay.DatePaid","UserGroup","StickyNotes"]}}';

  var parsedJSON = JSON.parse(netoString);
  	  parsedJSON.Filter.OrderID[0] = netoOrderID;
  return parsedJSON;
}


