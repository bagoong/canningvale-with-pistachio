/**
  * @name : pistachio-connect-platform-lib.js
  * @desc : handles all the outbound settings for integrations in an object interface
  * @desc : all the settings get stored in an object for the specific platform and accessed via keys
  * @desc Date Created : 2017-01-29
  * @desc Created by David Imrie
  * @desc Version 1.0
**/

var webserviceActions = {
      // neto ecommerce platform
		platformsettings :
      {
          // human type records
        contact   :
          {
              create     : {action : 'create-contact', method : 'POST',id : '1', url : ''},
              edit  		 : {action : 'update-contact', method : 'POST',id:'2', url : ''}
          },
          customer :
            {
                create   : {action : 'create-customer', method : 'POST', id : '1', url : ''},
                edit  	 : {action : 'update-customer', method : 'POST',id:'2', url: ''},
            },
          employee :
            {
                create   : {action : 'create-customer', method : 'POST',id:'1', url : ''},
                edit  	 : {action : 'update-customer', method : 'POST',id:'2', url : ''}
            },
          // end human type records

          // item records
          assemblyitem :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product', method : 'POST',id:'2', url : ''}
            },
          discountitem :
            {
                create     : {action : 'create-discount', method : 'POST',id:'1', url : ''},
                edit  : {action : 'edit-discount', method : 'POST',id:'2', url : ''}
            },
          downloaditem :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product', method : 'POST',id:'2', url : ''}
            },
          giftcertificateitem :
            {
                create     : {action : 'create-discount', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-discount', method : 'POST',id:'2', url : ''}
            },
          inventoryitem :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product', method : 'POST',id:'2', url : ''}
            },
          itemgroup :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product', method : 'POST',id:'2', url : ''}
            },
          lotnumberedinventoryitem :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product', method : 'POST',id:'2', url : ''}
            },
        kititem :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product',method : 'POST',id:'2', url : ''}
            },
          serializedinventoryitem :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product', method : 'POST',id:'2', url : ''}
            },
          serviceitem :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product', method : 'POST',id:'2', url : ''}
            },
          noninventoryitem :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product', method : 'POST',id:'2', url : ''}
            },
          itemfulfillment :
            {
                create     : {action : 'create-item-fulfillment', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-item-fulfillment',method : 'POST',id:'2', url : ''}
            },

          inventoryadjustment :
            {
                create     : {action : 'create-inventory-adjustment', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-inventory-adjustment',method : 'POST',id:'2', url : ''}
            },
          returnauthorization :
            {
                create     : {action : 'create-ra', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-ra',method : 'POST',id:'2', url : ''}
            },
	        otherchargeitem :
            {
                create     : {action : 'create-product', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-product', method : 'POST',id:'2', url : ''}
            },
	        salesorder :
            {
                create     : {action : 'create-salesorder', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-salesorder', method : 'POST',id:'2', url : ''}
            },
	        invoice :
            {
                create     : {action : 'create-invoice', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-invoice', method : 'POST',id:'2', url : ''}
            },
	        estimate :
            {
                create     : {action : 'create-invoice', method : 'POST',id:'1', url : ''},
                edit  : {action : 'update-invoice', method : 'POST',id:'2', url : ''}
            }
      },
			get : function(platform,nsAction){
			var returnObj = {};
          try
          {
            //platform = platform.replace(/[^A-Z0-9]/ig, "").toLowerCase();
            returnObj = webserviceActions['platformsettings'][nsAction];
          }
          catch(err)
          {
          }
					return returnObj;
			}
};
