function getPlatformOptions(settingsObj,platform,action,optionsObj,recType)
{
  var returnObj = {};
  switch(platform)
  {
    case '1':           /* wooCommerce */
      // load the actions from the webservicesActions object specifically to Neto
      var wsActions         = webserviceActions.get(settingsObj.getFieldText('custrecord_pm_int_platform_list'),recType);
      returnObj.headers  = {'netoapi_action': wsActions[action].action ,'netoapi_key':  settingsObj.getFieldValue('custrecord_pm_int_api_key'),'netoapi_username': settingsObj.getFieldValue('custrecord_pm_int_username') ,'cache-control':'no-cache','user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:31.0) Gecko/20100101 Firefox/31.0','content-type':'text/plain;charset=UTF-8'};
      returnObj.endPoint = settingsObj.getFieldValue('custrecord_pm_int_test_cust_endpoint');
      returnObj.method   = wsActions[action].method;
      returnObj.action   = wsActions[action].action;
      if(wsActions[action.url])
      {
        returnObj.url      = wsActions[action.url] ;
      }
      else
      {
        returnObj.url      = settingsObj.getFieldValue('custrecord_pm_int_api_endpoint') + '/wp-json/wc/v1/' + wsActions[action].action;
      }
    break;

    case '2':           /* shopify */
    break;

    case '3':           /* shopify plus */
    break;

    case '4':           /* BigCommerce */
    break;

    case '5':           /* neto */
      // load the actions from the webservicesActions object specifically to Neto
      var wsActions         = webserviceActions.get(settingsObj.getFieldText('custrecord_pm_int_platform_list'),recType);
      returnObj.headers  = {'netoapi_action': wsActions[action].action ,'netoapi_key':  settingsObj.getFieldValue('custrecord_pm_int_api_key'),'netoapi_username': settingsObj.getFieldValue('custrecord_pm_int_username') ,'cache-control':'no-cache','user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:31.0) Gecko/20100101 Firefox/31.0','content-type':'text/plain;charset=UTF-8'};
      returnObj.endPoint = settingsObj.getFieldValue('custrecord_pm_int_test_cust_endpoint');
      returnObj.method   = wsActions[action].method;
      returnObj.action   = wsActions[action].action;
      if(wsActions[action.url])
      {
        returnObj.url      = wsActions[action.url];
      }
      else
      {
        returnObj.url      = settingsObj.getFieldValue('custrecord_pm_int_api_endpoint');
      }
    break;

    case '6':         /*volusion */
    break;

    case '7':         /* lemonstand */
    break;

    case '8':         /* Magento 1.8 */
    break;

    case '9':         /* Magento 1.9 */
    break;

    case '10':        /* Magento 2.0 */
    break;

    case '11':        /* XMS One */
      // load the actions from the webservicesActions object specifically to Neto
      var wsActions         = webserviceActions.get(settingsObj.getFieldText('custrecord_pm_int_platform_list'),recType);
      returnObj.headers  = {'cache-control':'no-cache','user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:31.0) Gecko/20100101 Firefox/31.0','content-type':'text/plain;charset=UTF-8'};
      returnObj.endPoint = settingsObj.getFieldValue('custrecord_pm_int_test_cust_endpoint');
      returnObj.method   = wsActions[action].method;
      returnObj.action   = wsActions[action].action;
      returnObj.url      = settingsObj.getFieldValue('custrecord_pm_int_api_endpoint') + '&a=' + wsActions[action].action + '&token=' + settingsObj.getFieldValue('custrecord_pm_int_api_key');
      return returnObj;
      break;

    case '12':        /* custom */
    break;
  }
      return returnObj;
}
// end function
