// this function extracts the required data from Netsuite for the record type and returns it back to the caller



function pmGetCustomObjects(recType,action,settings,data)
{
  //  recType 		= the record being used
  //  action  		= created,edit,delete
  //  settings 		= the settings for the integration
  //  data 			= the actual record being processed
  var returnObj = {};

  // load customer record from contact if available
  if(recType === 'contact')
      {
		var custID = data.getFieldValue('company');
		if(custID)
          {
          var customerRecord = nlapiLoadRecord('customer',custID );
		  if(customerRecord)
	          {
    	        returnObj.customer = JSON.parse(JSON.stringify(customerRecord));
	          }
          }
      }
  	// end loading customer record for contact

  return returnObj;
 }





// deprecated function 
function getCustomObjects(recType,type,settingsObj,record)
{
  //  recType = the record being used
  //  type =
  //  settingsObj = the settings for the integration
  //  record = the actual record being processed
  var returnObj = {};

  return returnObj;
 }







function loadItem(id){
  var CONST_ITEMTYPE = {
    'Assembly' : 'assemblyitem',
    'Description' : 'descriptionitem',
    'Discount' : 'discountitem',
    'GiftCert' : 'giftcertificateitem',
    'InvtPart' : 'inventoryitem',
    'Group' : 'itemgroup',
    'Kit' : 'kititem',
    'Markup' : 'markupitem',
    'NonInvtPart' : 'noninventoryitem',
    'OthCharge' : 'otherchargeitem',
    'Payment' : 'paymentitem',
    'Service' : 'serviceitem',
    'Subtotal' : 'subtotalitem'
  };

  try {
    return nlapiLoadRecord(CONST_ITEMTYPE[nlapiLookupField('item', id, 'type')], id);
  }
  catch(e) {
    nlapiLogExecution('ERROR','loadItem failed with error: id:' + id, e.message);
  }
}
// end function