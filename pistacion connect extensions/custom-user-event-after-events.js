// this function fires after a user event. returns void
// suitable for updating a record or sending a notification

function pmAfterEventCustomFunction(recType,settings,data,responseBody)
{
  //  recType 		= the record being used
  //  settings 		= the settings for the integration
  //  data 			= the actual record being processed
  //  responseBody	= the response from the external system
}
// end function

// deprecated
function afterEventCustomFunction(recType,type,settingsObj,record)
{
  //  recType = the record being used
  //  type =
  //  settingsObj = the settings for the integration
  //  record = the actual record being processed
 }
