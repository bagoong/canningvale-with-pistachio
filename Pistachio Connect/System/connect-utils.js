/*
Utils library to contain all the utilities for the integration kit
* dLog()					-- logs data to the Netsuite account
* dErr()					-- logs an error to the Netsuite account
* setValue()				-- helper to set a value, if null, sets an empty value
* isEmpty()					-- helper to detect if the field is emptyjoi
*
* Update Log
* 2017-08-12 				-- Updated to fix GST calculation to 8 decimal places
*/

// determines if the string should be Netsuite T or F
function netsuiteTrueFalse(string)
{
	if(string == '1')
	{
		return 'T';
	}
	else
	{
		return 'F';
	}
}
// end function

// calculates the taxes being included into the request
function calculateTax(inboundItemTaxRate,price)
{
	if(inboundItemTaxRate == '10')
	{
		var taxRate = 1.1;
		var price   = price
		var exTax   = price / taxRate;
			exTax 	= exTax.toFixed(2);
	}
return exTax;
}
// end function


function dLog(logTitle, logDetails)
{
nlapiLogExecution('DEBUG', logTitle, logDetails);
}
// end function

function dErr(logTitle, logDetails)
{
	nlapiLogExecution('ERROR', logTitle, logDetails);
}
// end function

function setValue(fldValue)
{
	if (isEmpty(fldValue))
		return '';

	return fldValue;
}
// end function

function isEmpty(fldValue)
{
	return fldValue === '' || fldValue === null || fldValue === undefined;
}
// end function


// checks if a field is empty or not
function checkIsEmpty(fldValue)
{
	if(fldValue == 'undefined')
	{
		return "";
	}
	if(fldValue == 'null')
	{	
		return "";
	}
	if(fldValue == '')
	{
		return "";
	}
}
// end function

function dollarFormat(x) {
    return x.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
// end function



// this function parses a caption against anything between %--% and replaces it with a nominated value
function parseCaption(textVar, valueToChange, replaceText) {
        var recordArray = textVar.split("%");    
        for (var i = 0; i < recordArray.length; i++) {
            if (recordArray[i] === valueToChange) {
                recordArray[i] = replaceText;
            }
        }
        return recordArray.join("%").replace(/[\[\]%]+/g, '');
}
// end function

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
// end function


function parseXML(dataObj,field)
{
	/* sample code for demonstrating how to parse XML value
	var xml = '<Currency CrossOrder="0" Kod="USD" CurrencyCode="USD"><Unit>1</Unit><Isim>ABD DOLARI</Isim><CurrencyName>US DOLLAR</CurrencyName></Currency>';
	var asDoc = nlapiStringToXML(xml);
	nlapiSelectValue(asDoc, 'Currency/@CurrencyCode');
	*/

	var xml = dataObj;
	var asDoc = nlapiStringToXML(xml);
	return nlapiSelectValue(asDoc,field);
}
// end function

// this function merges 2 objects together
function extend(obj, src) {
    Object.keys(src).forEach(function(key) { obj[key] = src[key]; });
    return obj;
}
// end function 

function getSavedSearch(integration,record,subType,searchFilters)
{
  /**
    * @desc gets all saved searches nominated against a record type 
    * @param - integration - the ID for the integration 
    * @param - record - The Record type being leveraged for the saved search 
    * @param - subType - the record subtype being leveraged 
    * @param - searchFilters - any additional search filters to be applied 
  */

  var searchID    = '';       // the search ID - internal ID if one is found.
  var returnObj   = {};       // return object for the search(s) to be returned 

  if(!subType)
  {
    subType = '';
  }

  var filters = [];
      filters.push(new nlobjSearchFilter('custrecord_pm_int_usr_search_int',null,'is',integration));
      filters.push(new nlobjSearchFilter('custrecord_pm_int_search_rec',null,'is',record));
      filters.push(new nlobjSearchFilter('custrecord_pm_int_inc_search',null,'is','1'));
      filters.push(new nlobjSearchFilter('custrecord_pm_int_rec_sub_type',null,'is',subType));
      filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));

  var columns = [];
      columns.push(new nlobjSearchColumn('custrecord_pm_int_usr_search_int'));
      columns.push(new nlobjSearchColumn('custrecord_pm_int_search_rec'));
      columns.push(new nlobjSearchColumn('custrecord_pm_int_search'));
      columns.push(new nlobjSearchColumn('custrecord_pm_int_rec_sub_type'));
      columns.push(new nlobjSearchColumn('custrecord_pm_int_inc_search'));
      columns.push(new nlobjSearchColumn('isinactive'));

      var results = nlapiSearchRecord('customrecord_pm_int_user_event_searches',null,filters,columns);
      
      // launch the search included and return its results 
      if(results)
      {        
          returnObj = processSavedSearch(results,searchFilters);
      } 
      //return returnObj;     // return the object and child arrays if collected 
return returnObj;
}
// end function 




function processSavedSearch(searches,filterArgs)
{
  /**
    * @desc - processes any saved searches and returns them back to the caller
    * @param searches - an array of search internal ID's to be processed 
    * @param filterArgs - an array of objects containing search criteria values 
  */

    var returnObj = {};
    returnObj.searchResults = [];
        for(i=0;i<=(searches.length-1);i++)
        {
        filters = [];
              // prepare any additional filter 
              
              if(filterArgs)
              {
                for(j=0;j<=(filterArgs.length-1);j++)
                {
                  if(filterArgs[j].field !== '')
                    {
                      if(filterArgs.id === '')
                      {
                        filters.push(new nlobjSearchFilter(filterArgs[j].field,null,filterArgs[j].filterArgs[j].opperator,filterArgs[j].value));
                      }
                        else
                      {
                        filters.push(new nlobjSearchFilter(filterArgs[j].field,filterArgs[j].id,filterArgs[j].opperator,filterArgs[j].value));
                      }
                    }
                }
              }
              // end dynamically adding filters 
              // end preparation of filters 
              searchID = searches[i].getValue('custrecord_pm_int_search');
              var searchRecord = nlapiSearchRecord(null,searchID,filters);
              if(searchRecord)
              {
                returnObj.searchResults.push(searchRecord);
              }              
        }
return returnObj;
}
// end function 


function getPlatformName(name)
{
  var returnString = name.replace(/[^A-Z0-9]/ig, "").toLowerCase();
  return returnString;
}
// end functions 

function incGSTAU(rate)
{
	if(! isNaN(parseFloat(rate)))
	{
		var gst   = 1.1;
		var inc   = parseFloat(rate) * gst;
		return inc.toFixed(8);		
	}
	else 
	{
		return 0.00;
	}
}

function GSTAU(rate)
{
	if(! isNaN(parseFloat(rate)))
	{
		var tax = 11;
		var gst  = parseFloat(rate) / tax;
		return gst.toFixed(8);		
	}
	else
	{
			return 0.00;
	}

}
// end function 


function exGSTAU(rate)
{
	if(! isNaN(parseFloat(rate)))
	{
		var tax = GSTAU(parseFloat(rate));
		var ex = rate - tax;
		return ex.toFixed(8);		
	}
	else 
	{
		return 0.00;
	}
}

// end australian tax functions

// NZ tax calculation functions

function incGSTNZ(rate)
{
if(! isNaN(parseFloat(rate)))
{
	var gst   = 1.15;
	var inc   = parseFloat(rate) * 3 / 23;
	return inc.toFixed(8);	
}
else 
{
	return 0.00;
}


}

function GSTNZ(rate)
{
	if(! isNaN(parseFloat(rate)))
	{
		var gst  = parseFloat(rate) * 3 / 23;
		return gst.toFixed(8);		
	}
	else 
	{
		return 0.00;
	}


}
// end function 


function exGSTNZ(rate)
{
	if(! isNaN(parseFloat(rate)))
	{
		var tax = GSTNZ(parseFloat(rate));
		var ex = parseFloat(rate) - tax;
		return ex.toFixed(8);		
	}
	else
	{
		return 0.00;
	}

}


// end NZ tax calculation functions


/**
 * Originally from http://davidwalsh.name/convert-xml-json
 * This is a version that provides a JSON object without the attributes and places textNodes as values
 * rather than an object with the textNode in it.
 * 27/11/2012
 * Ben Chidgey
 *
 * @param xml
 * @return {*}
 */
function xmlToJson(xml) {

    // Create the return object
    var obj = {};

    // text node
    if (4 === xml.nodeType) {
        obj = xml.nodeValue;
    }

    if (xml.hasChildNodes()) {
        for (var i = 0; i < xml.childNodes.length; i++) {
            var TEXT_NODE_TYPE_NAME = '#text',
                item = xml.childNodes.item(i),
                nodeName = item.nodeName,
                content;

            if (TEXT_NODE_TYPE_NAME === nodeName) {
                //single textNode or next sibling has a different name
                if ((null === xml.nextSibling) || (xml.localName !== xml.nextSibling.localName)) {
                    content = xml.textContent;

                //we have a sibling with the same name
                } else if (xml.localName === xml.nextSibling.localName) {
                    //if it is the first node of its parents childNodes, send it back as an array
                    content = (xml.parentElement.childNodes[0] === xml) ? [xml.textContent] : xml.textContent;
                }
                return content;
            } else {
                if ('undefined' === typeof(obj[nodeName])) {
                    obj[nodeName] = xmlToJson(item);
                } else {
                    if ('undefined' === typeof(obj[nodeName].length)) {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }

                    obj[nodeName].push(xmlToJson(item));
                }
            }
        }
    }
    return obj;
}