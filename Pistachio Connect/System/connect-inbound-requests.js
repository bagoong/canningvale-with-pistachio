/**
  * @name : connect-inbound-requests.js
  * @desc : Creates the requried records within Netsuite on demand
  * @desc :
  * @desc Date Created : 2017-02-07
  * @desc Created by David Imrie
  * @desc Version 1.0
  * @name : createSalesOrder
  * @name : createCustomer
  * @name : createDeposit
  * @name : createCashSale
  * @name : createQuote 
  * Log Tracking Code Numbers 
  * 1 - Success
  * 2 - Fail
  * 3 - Already Exists 
  * 4 - Raw Data 
  * 5 - Error
  * 6 - Prepared Record Data
  * Updated Notes 
  * 2017-06-01 - Updated to enhance logging within the record creation Process
  * 2017-06-02 - Updated to remove some of the additional logging that is not needed 
  * 2017-06-07 - Updated to allow for a forced override of a sales order
  * 2017-09-16 - Updated to handle updating of customer with additional parameter for step to process external id
*/

// creates a salesorder
function createSalesOrder(objData,settingsObj,step)
{
  // preparation of return object
    dLog('START', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CREATE SALES ORDER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    var rec                               = {};
    var objResponseData 				          = {};
    var customerID                        = '';
    objResponseData.status 				        = "success";
    objResponseData.responsemessage 	    = "";
    objResponseData.responsecode 		      = "00";
    objResponseData.data 				          = '';
    objResponseData.internalid 		        = '';
    objResponseData.entityid 			        = '';
    // end preparation of return object  

    try
    {
    var soObj                 = objData.data[0];
        customerID            = soObj.entityid;
        // log the raw data as its ready for the transaction creation process        
    if(soObj.email)
          {
          var filters = [];
              filters.push(new nlobjSearchFilter('email',null,'is',soObj.email));
          var columns = [];
              columns.push(new nlobjSearchColumn('internalid'));
              columns.push(new nlobjSearchColumn('email'));
          var results = nlapiSearchRecord('customer',null,filters,columns);
            if(results)
            {
              customerID = results[(results.length-1)].getValue('internalid');
            }
          }
        // end searching for a customer ID match 
    var transactionType = 'salesorder';
    if(settingsObj.getFieldValue('custrecord_pm_int_transform_sales_order') === '1')
    {
      transactionType = 'cashsale';
    }
    if(step)
    {
      transactionType = 'estimate';
    }

    if(soObj.transformifquote == 'T')
    {
    if(soObj.transformifquotenumber)
          {
          var isEstimate = checkIfEstimate(soObj.transformifquotenumber);
          if(isEstimate.status === true)
            { 
            rec = nlapiTransformRecord('estimate',isEstimate.internalid,'salesorder');
            rec.setFieldValue('externalid',soObj.externalid);
            }
          else
            {
            rec = nlapiCreateRecord(transactionType);
            rec.setFieldValue('externalid',soObj.externalid);
            }
          // end 
          }
      else
      {
            rec = nlapiCreateRecord(transactionType);
            rec.setFieldValue('externalid',soObj.externalid);
      }
      // end
    }
    else
    {
    rec = nlapiCreateRecord(transactionType);      
    rec.setFieldValue('externalid',soObj.externalid);
    }
    // record creation type

    rec.setFieldValue('entity', setValue(customerID)); // entity id is passed in by default.
    rec.setFieldValue('customform',settingsObj.getFieldValue('custrecord_pm_int_sales_order_form'));
    rec.setFieldValue('leadsource',setValue(soObj.leadsource));
    // override the address block if nominated 
    if(soObj.usecustomeraddress)
    {
      if(soObj.usecustomeraddress == 'F')
      {
        // override the address with a custom address value 
        rec.setFieldValue('billaddress',setValue(soObj.billingaddress));
        rec.setFieldValue('shipaddress',setValue(soObj.shippingaddress));              
      }      
      else
      {
        rec.setFieldValue('billaddresslist',setValue(soObj.billaddresslist));
        rec.setFieldValue('shipaddresslist',setValue(soObj.shipaddresslist));              
      }
    }
    // otherwise use the default address nominated on the transaction 
    else
    {
      rec.setFieldValue('billaddresslist',setValue(soObj.billaddresslist));
      rec.setFieldValue('shipaddresslist',setValue(soObj.shipaddresslist));
    }
    // end applying it 

    rec.setFieldValue('location',setValue(soObj.location));
    rec.setFieldValue('currency',setValue(soObj.currency));
    rec.setFieldValue('class',setValue(soObj.class));
    rec.setFieldValue('department',setValue(soObj.department));
    rec.setFieldValue('salesrep',setValue(soObj.salesrep));
    rec.setFieldValue('shipmethod',setValue(soObj.shipmethod));

    // deduct tax from shipping items 
    if(settingsObj.getFieldValue('custrecord_pm_int_ded_tax_ship_item'))
    {
      if(settingsObj.getFieldValue('custrecord_pm_int_inbound_tax_rate') === '1')
      {
      soObj.shippingcost = exGSTAU(soObj.shippingcost);
      }
      else
      {
      soObj.shippingcost = exGSTNZ(parseFloat(soObj.shippingcost));
      }
    }
    // end deduct tax from shipping
    if(soObj.shippingcost)
    {
    rec.setFieldValue('shippingcost',soObj.shippingcost);      
    }

    // deduct tax from discount items 
    if(settingsObj.getFieldValue('custrecord_pm_int_ded_disc_tax'))
    {
        if(settingsObj.getFieldValue('custrecord_pm_int_inbound_tax_rate') === '1')
        {
        soObj.discountrate = exGSTAU(parseFloat(soObj.discountrate));
        }
        else
        {
        soObj.discountrate = exGSTNZ(parseFloat(soObj.discountrate));
        }
    }
    if(soObj.discountrate)
    {
      var discount       = soObj.discountrate;
      var discountAmount = discount.replace('--','-');

      rec.setFieldValue('discountrate',setValue(discountAmount));
      rec.setFieldValue('discountitem',setValue(soObj.discount));
    }
    // end discount rate adjustments 

    rec.setFieldValue('istaxable',setValue(soObj.istaxable));
    rec.setFieldValue('paymentmethod',   	settingsObj.getFieldValue('custrecord_pm_int_payment_method'));
    // rec.setFieldValue('terms','4');  -- removed because its unknown why its actually here 
    var arrItems              = soObj.items;
    // SO Items

    for ( var i = 0; i < arrItems.length; i++)
    {
      dLog('Line Items', 'Item:' + arrItems[i].internalid + ' Quantity ' + arrItems[i].quantity );
      rec.selectNewLineItem('item');
      rec.setCurrentLineItemValue('item', 'item', setValue(arrItems[i].internalid));
      rec.setCurrentLineItemValue('item', 'quantity', setValue(arrItems[i].quantity));
      var arrCustomCols = arrItems[i].custom_fields;
      for (var x in arrCustomCols)
      {
        rec.setCurrentLineItemValue('item', arrCustomCols[x].name, arrCustomCols[x].value);
      }
      rec.commitLineItem('item');
    }
      /* custom fields section */
      var arrCustomFields = soObj.custom_fields;		// custom_fields array on the suitelet.
      //var arrCustomFields = custFields[i].custom_fields;

      for ( var j = 0; j < arrCustomFields.length; j++)
      {
        rec.setFieldValue(arrCustomFields[j].name, arrCustomFields[j].value);
      }
      /* end custom fields section */
      rec.setFieldValue('custbody_pm_int_webstore',settingsObj.getFieldValue('id'));

      // record payment if it is a cash sale 
      if(transactionType === 'cashsale')
      {
        if(settingsObj.getFieldValue('custrecord_pm_rec_cash_sale_dep') === '1')
        {
          rec.setFieldValue('undepfunds','F');
          rec.setFieldValue('account',settingsObj.getFieldValue('custrecord_pm_int_cash_sale_acc'));          
        }
      }
      // end cash sale deposit payment 

      // log the newly created record data
      // create the sales order
      var salesorderID = nlapiSubmitRecord(rec, true, true);

      if(salesorderID)
      {
        // log the successfully created sales order 
        logInboundRequests('1',settingsObj.getFieldValue('id'), 'Sales Order Internal ID \n' + salesorderID );                    
        var custSalesOrderAction = customSalesOrderAction('create',salesorderID);
        dLog('Sales Order', salesorderID);
      }
      // end custom function inclusion 
      // log the raw data
      objResponseData.internalid = salesorderID;

    /* automatically record a deposit payment if nominated */
    //dLog('deposit type selected',settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep'));
    if(! step )
    {    
    if(settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep') == '1' || settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep')  == '3' )
    {
        // if based on using Template 
        if(settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep')  == '3')
        { 
            //dLog('deposit',settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep') );
            if(! soObj.recorddeposit)
            {
            //dLog('deposit','!' );              
              return objResponseData;
            } 
            else 
            {
              if(soObj.recorddeposit == 'F')
              {
                  //dLog('deposit','F' );              
                  return objResponseData;
              }
            }
            //dLog('deposit','3 - made the final step' );              
        }
        // end recording deposit process 
        var soId 	= salesorderID;
        var soRec 	= nlapiLoadRecord('salesorder',soId);
        var record 	= nlapiCreateRecord('customerdeposit', {entity:soRec.getFieldValue('entity'), salesorder:soId});
            //dLog('deposit','Recording a deposit' );              
        if(soObj.deposit_customform)
          {
            record.setFieldValue('customform',        setValue(soObj.deposit_customform));          
          }
          else
          {
            record.setFieldValue('customform',        settingsObj.getFieldValue('custrecord_pm_int_dep_form'));
          }

          if(soObj.deposit_paymentmethod)
          {
            record.setFieldValue('paymentmethod',     setValue(soObj.deposit_paymentmethod));            
          }
          else 
          {
            record.setFieldValue('paymentmethod',     settingsObj.getFieldValue('custrecord_pm_int_dep_pay_meth'));            
          }

          if(soObj.deposit_location)
          {
            record.setFieldValue('location',          setValue(soObj.deposit_location));            
          }
          else 
          {
            record.setFieldValue('location',          settingsObj.getFieldValue('custrecord_pm_int_dep_location'));            
          }

          if(soObj.deposit_department)
          {
            record.setFieldValue('department',        setValue(soObj.deposit_department));
          }
          else 
          {
            record.setFieldValue('department',        settingsObj.getFieldValue('custrecord_pm_int_dep_depart'));            
          }

          if(soObj.deposit_class)
          {
            record.setFieldValue('class',             setValue(soObj.deposit_class));
          }
          else 
          {
            record.setFieldValue('class',             settingsObj.getFieldValue('custrecord_pm_int_deposit_class'));
          }

          // cant change these
          record.setFieldValue('customer',			    customerID);
          record.setFieldValue('salesorder',			  salesorderID);
          // end cant chage these

          if(settingsObj.getFieldValue('custrecord_pm_int_partial_dep') === '1')
          {
            //setValue(soObj.grandTotal)
            if(soObj.deposit[0].payment)
            {
              record.setFieldValue('payment',           setValue(soObj.deposit[0].payment)); 
            }
            else 
            {
              record.setFieldValue('payment',           setValue(soObj.grandtotal));
            }
          }

          // connect integration ID fields
          /* custom fields section */
    			var depCustomFields = soObj.deposit[0].custom_fields;
      			for ( var n = 0; n< depCustomFields.length; n++)
      			{
    				record.setFieldValue(depCustomFields[n].name, depCustomFields[n].value);
    			  }
    			/* end custom fields section */

          // if payment is via paypal, align it to the paypal account
            if(settingsObj.getFieldValue('custrecord_pm_int_dep_acc'))
            {
              record.setFieldValue('undepfunds',			 'F');
                  if(soObj.paypal === 'true')
                  {
                  record.setFieldValue('account',				    settingsObj.getFieldValue('custrecord_pm_int_paypal_account'));
                  }
                  else
                  {
                        // if account id is nominated, use it, otherwise default back to settings defined
                        if(soObj.deposit[0].account)
                        {
                          record.setFieldValue('account',   setValue(soObj.deposit[0].account) );
                        }
                        else 
                        {
                          record.setFieldValue('account',           settingsObj.getFieldValue('custrecord_pm_int_dep_acc'));
                        }
                  }
            }
            else 
            {
              record.setFieldValue('undepfunds',			 'T');
            }
            // log the record data 
            depositID 									      = nlapiSubmitRecord(record,true,true);

            // log the successful deposit 
            if(depositID)
            { 
              dLog('Deposit', depositID);                    
              logInboundRequests('1',settingsObj.getFieldValue('id'), 'Customer Deposit Internal ID \n' + depositID );              
              var custDepositAction             = customDepositAction('create',depositID);
            }
            objResponseData.depositID	 				= depositID;
            objResponseData.entityid 					= customerID;
    }
}
    /* // end recording a deposit payment if nominated */
    /* // end create cash sale conversion */
  }
  catch (e)
  {
    var stErrMsg = '';
    if (e.getDetails !== undefined)
    {
      stErrMsg = e.getCode() + ' ' + e.getDetails() + ' ' + e.getStackTrace();
      // error logging 5 - error
      logInboundRequests('5',settingsObj.getFieldValue('id'), 'Error Occured - Undefined Error \n' +  stErrMsg );  
      // log the error 
      dLog('Error Occured',stErrMsg);
    }
    else
    {
      stErrMsg = e.toString();
      dLog('Error Occured',stErrMsg);
    }
    // log that the transaction failed 
    // end logging that the transaction failed 
    objResponseData.status              = "FAILED";
    objResponseData.responsemessage 	  = stErrMsg;
    objResponseData.responsecode 		    = "99";
    objResponseData.internalid 		      = '';
    logInboundRequests('5',settingsObj.getFieldValue('id'), 'Error Occured \n' +  stErrMsg );      
  }
  dLog('END', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CREATE SALES ORDER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  return objResponseData;
}
// end create sales order function






// creates a salesorder
function updateSalesOrder(objData,settingsObj,step)
{
    dLog('START', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UPDATE SALES ORDER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  // preparation of return object
    var rec                               = {};
    var objResponseData 				          = {};
    var customerID                        = '';
    objResponseData.status 				        = "success";
    objResponseData.responsemessage 	    = "";
    objResponseData.responsecode 		      = "00";
    objResponseData.data 				          = '';
    objResponseData.internalid 		        = '';
    objResponseData.entityid 			        = '';
    // end preparation of return object  

    try
    {
    var soObj                 = objData.data[0];
        customerID            = soObj.entityid;
        // log the raw data as its ready for the transaction creation process        
    if(soObj.email)
          {
          var filters = [];
              filters.push(new nlobjSearchFilter('email',null,'is',soObj.email));
          var columns = [];
              columns.push(new nlobjSearchColumn('internalid'));
              columns.push(new nlobjSearchColumn('email'));
          var results = nlapiSearchRecord('customer',null,filters,columns);
            if(results)
            {
              customerID = results[(results.length-1)].getValue('internalid');
            }
          }
        // end searching for a customer ID match 
    var transactionType = 'salesorder';
    if(settingsObj.getFieldValue('custrecord_pm_int_transform_sales_order') === '1')
    {
      transactionType = 'cashsale';
    }
    if(step)
    {
      transactionType = 'estimate';
    }
        rec = nlapiLoadRecord(transactionType, soObj.internalid);      
      //rec.setFieldValue('externalid',soObj.externalid);

    // record creation type
    rec.setFieldValue('entity', setValue(customerID)); // entity id is passed in by default.
    rec.setFieldValue('customform',settingsObj.getFieldValue('custrecord_pm_int_sales_order_form'));
    rec.setFieldValue('leadsource',setValue(soObj.leadsource));
    // override the address block if nominated 
    if(soObj.usecustomeraddress)
    {
      if(soObj.usecustomeraddress == 'F')
      {
        // override the address with a custom address value 
        rec.setFieldValue('billaddress',setValue(soObj.billingaddress));
        rec.setFieldValue('shipaddress',setValue(soObj.shippingaddress));              
      }      
      else
      {
        rec.setFieldValue('billaddresslist',setValue(soObj.billaddresslist));
        rec.setFieldValue('shipaddresslist',setValue(soObj.shipaddresslist));              
      }
    }
    // otherwise use the default address nominated on the transaction 
    else
    {
      rec.setFieldValue('billaddresslist',setValue(soObj.billaddresslist));
      rec.setFieldValue('shipaddresslist',setValue(soObj.shipaddresslist));
    }
    // end applying it 

    rec.setFieldValue('location',setValue(soObj.location));
    rec.setFieldValue('currency',setValue(soObj.currency));
    rec.setFieldValue('class',setValue(soObj.class));
    rec.setFieldValue('department',setValue(soObj.department));
    rec.setFieldValue('salesrep',setValue(soObj.salesrep));
    rec.setFieldValue('shipmethod',setValue(soObj.shipmethod));

    // deduct tax from shipping items 
    if(settingsObj.getFieldValue('custrecord_pm_int_ded_tax_ship_item'))
    {
      if(settingsObj.getFieldValue('custrecord_pm_int_inbound_tax_rate') === '1')
      {
      soObj.shippingcost = exGSTAU(soObj.shippingcost);
      }
      else
      {
      soObj.shippingcost = exGSTNZ(parseFloat(soObj.shippingcost));
      }
    }
    // end deduct tax from shipping
    if(soObj.shippingcost)
    {
    rec.setFieldValue('shippingcost',soObj.shippingcost);      
    }

    // deduct tax from discount items 
    if(settingsObj.getFieldValue('custrecord_pm_int_ded_disc_tax'))
    {
        if(settingsObj.getFieldValue('custrecord_pm_int_inbound_tax_rate') === '1')
        {
        soObj.discountrate = exGSTAU(parseFloat(soObj.discountrate));
        }
        else
        {
        soObj.discountrate = exGSTNZ(parseFloat(soObj.discountrate));
        }
    }
    if(soObj.discountrate)
    {
      var discount       = soObj.discountrate;
      var discountAmount = discount.replace('--','-');

      rec.setFieldValue('discountrate',setValue(discountAmount));
      rec.setFieldValue('discountitem',setValue(soObj.discount));
    }
    // end discount rate adjustments 

    rec.setFieldValue('istaxable',setValue(soObj.istaxable));
    rec.setFieldValue('paymentmethod',   	settingsObj.getFieldValue('custrecord_pm_int_payment_method'));
    // rec.setFieldValue('terms','4');  -- removed because its unknown why its actually here 
    var arrItems              = soObj.items;
    // SO Items

    // remove items from the sales order 
    var lineCount = rec.getLineItemCount ('item' ); 
        for (var k = lineCount; k>=1; k--) 
        {
          rec.removeLineItem ('item',k);
        }
    // end removing item 

    for ( var i = 0; i < arrItems.length; i++)
    {
      dLog('Line Items', 'Item:' + arrItems[i].internalid + ' Quantity ' + arrItems[i].quantity );
      rec.selectNewLineItem('item');
      rec.setCurrentLineItemValue('item', 'item', setValue(arrItems[i].internalid));
      rec.setCurrentLineItemValue('item', 'quantity', setValue(arrItems[i].quantity));
      var arrCustomCols = arrItems[i].custom_fields;
      for (var x in arrCustomCols)
      {
        rec.setCurrentLineItemValue('item', arrCustomCols[x].name, arrCustomCols[x].value);
      }
      rec.commitLineItem('item');
    }
      /* custom fields section */
      var arrCustomFields = soObj.custom_fields;		// custom_fields array on the suitelet.
      //var arrCustomFields = custFields[i].custom_fields;

      for ( var j = 0; j < arrCustomFields.length; j++)
      {
        rec.setFieldValue(arrCustomFields[j].name, arrCustomFields[j].value);
      }
      /* end custom fields section */
      rec.setFieldValue('custbody_pm_int_webstore',settingsObj.getFieldValue('id'));

      // record payment if it is a cash sale 
      if(transactionType === 'cashsale')
      {
        if(settingsObj.getFieldValue('custrecord_pm_rec_cash_sale_dep') === '1')
        {
          rec.setFieldValue('undepfunds','F');
          rec.setFieldValue('account',settingsObj.getFieldValue('custrecord_pm_int_cash_sale_acc'));          
        }
      }
      // end cash sale deposit payment 

      // log the newly created record data
      // create the sales order
      var salesorderID = nlapiSubmitRecord(rec, true, true);

      if(salesorderID)
      {
        dLog('Sales Order', salesorderID);
        // log the successfully created sales order 
        logInboundRequests('1',settingsObj.getFieldValue('id'), 'Sales Order Internal ID \n' + salesorderID );                    
        var custSalesOrderAction = customSalesOrderAction('create',salesorderID);
      }
      // end custom function inclusion 
      // log the raw data
      objResponseData.internalid = salesorderID;

    /* automatically record a deposit payment if nominated */
    //dLog('deposit type selected',settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep'));
    if(! step )
    {    
    if(settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep') == '1' || settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep')  == '3' )
    {
        // if based on using Template 
        if(settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep')  == '3')
        { 
            //dLog('deposit',settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep') );
            if(! soObj.recorddeposit)
            {
            //dLog('deposit','!' );              
              return objResponseData;
            } 
            else 
            {
              if(soObj.recorddeposit == 'F')
              {
                  //dLog('deposit','F' );              
                  return objResponseData;
              }
            }
            //dLog('deposit','3 - made the final step' );              
        }
        // end recording deposit process 

        var soId 	= salesorderID;
        var soRec 	= nlapiLoadRecord('salesorder',soId);

        // disabling of the deposit process 




        var record 	= nlapiCreateRecord('customerdeposit', {entity:soRec.getFieldValue('entity'), salesorder:soId});           
        if(soObj.deposit_customform)
          {
            record.setFieldValue('customform',        setValue(soObj.deposit_customform));          
          }
          else
          {
            record.setFieldValue('customform',        settingsObj.getFieldValue('custrecord_pm_int_dep_form'));
          }

          if(soObj.deposit_paymentmethod)
          {
            record.setFieldValue('paymentmethod',     setValue(soObj.deposit_paymentmethod));            
          }
          else 
          {
            record.setFieldValue('paymentmethod',     settingsObj.getFieldValue('custrecord_pm_int_dep_pay_meth'));            
          }

          if(soObj.deposit_location)
          {
            record.setFieldValue('location',          setValue(soObj.deposit_location));            
          }
          else 
          {
            record.setFieldValue('location',          settingsObj.getFieldValue('custrecord_pm_int_dep_location'));            
          }

          if(soObj.deposit_department)
          {
            record.setFieldValue('department',        setValue(soObj.deposit_department));
          }
          else 
          {
            record.setFieldValue('department',        settingsObj.getFieldValue('custrecord_pm_int_dep_depart'));            
          }

          if(soObj.deposit_class)
          {
            record.setFieldValue('class',             setValue(soObj.deposit_class));
          }
          else 
          {
            record.setFieldValue('class',             settingsObj.getFieldValue('custrecord_pm_int_deposit_class'));
          }

          // cant change these
          record.setFieldValue('customer',			    customerID);
          record.setFieldValue('salesorder',			  salesorderID);
          // end cant chage these

          if(settingsObj.getFieldValue('custrecord_pm_int_partial_dep') === '1')
          {
            //setValue(soObj.grandTotal)
            if(soObj.deposit[0].payment)
            {
              record.setFieldValue('payment',           setValue(soObj.deposit[0].payment)); 
            }
            else 
            {
              record.setFieldValue('payment',           setValue(soObj.grandtotal));
            }
          }

          // connect integration ID fields
          /* custom fields section */
    			var depCustomFields = soObj.deposit[0].custom_fields;
      			for ( var n = 0; n< depCustomFields.length; n++)
      			{
    				record.setFieldValue(depCustomFields[n].name, depCustomFields[n].value);
    			  }
    			/* end custom fields section */

          // if payment is via paypal, align it to the paypal account
            if(settingsObj.getFieldValue('custrecord_pm_int_dep_acc'))
            {
              record.setFieldValue('undepfunds',			 'F');
                  if(soObj.paypal === 'true')
                  {
                  record.setFieldValue('account',				    settingsObj.getFieldValue('custrecord_pm_int_paypal_account'));
                  }
                  else
                  {
                        // if account id is nominated, use it, otherwise default back to settings defined
                        if(soObj.deposit[0].account)
                        {
                          record.setFieldValue('account',   setValue(soObj.deposit[0].account) );
                        }
                        else 
                        {
                          record.setFieldValue('account',           settingsObj.getFieldValue('custrecord_pm_int_dep_acc'));
                        }
                  }
            }
            else 
            {
              record.setFieldValue('undepfunds',			 'T');
            }
            // log the record data 
            // depositID 									      = nlapiSubmitRecord(record,true,true);

            // log the successful deposit 
            if(depositID)
            {
              dLog('Deposit', depositID);
              logInboundRequests('1',settingsObj.getFieldValue('id'), 'Customer Deposit Internal ID \n' + depositID );              
              // var custDepositAction             = customDepositAction('create',depositID);
            }
            //objResponseData.depositID	 				= depositID;
            //objResponseData.entityid 					= customerID;
    }
}
    /* // end recording a deposit payment if nominated */
    /* // end create cash sale conversion */
  }
  catch (e)
  {
    var stErrMsg = '';
    if (e.getDetails !== undefined)
    {
      stErrMsg = e.getCode() + ' ' + e.getDetails() + ' ' + e.getStackTrace();
      // error logging 5 - error
      logInboundRequests('5',settingsObj.getFieldValue('id'), 'Error Occured - Undefined Error \n' +  stErrMsg );  
      // log the error 
      dLog('Error occured',stErrMsg);
    }
    else
    {
      stErrMsg = e.toString();
      dLog('error occured',stErrMsg);
    }
    // log that the transaction failed 
    // end logging that the transaction failed 
    objResponseData.status              = "FAILED";
    objResponseData.responsemessage 	  = stErrMsg;
    objResponseData.responsecode 		    = "99";
    objResponseData.internalid 		      = '';
    logInboundRequests('5',settingsObj.getFieldValue('id'), 'Error Occured \n' +  stErrMsg );      
  }
  dLog('END', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UPDATE SALES ORDER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  return objResponseData;
}
// end create sales order function







// creates a salesorder
function createEstimate(objData,settingsObj,step)
{
    dLog('START', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CREATE ESTIMATE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    // preparation of return object
    var objResponseData 				          = {};
    var customerID                        = '';
    objResponseData.status 				        = "success";
    objResponseData.responsemessage 	    = "";
    objResponseData.responsecode 		      = "00";
    objResponseData.data 				          = '';
    objResponseData.internalid 		        = '';
    objResponseData.entityid 			        = '';
    // end preparation of return object

  try
  {
    var soObj                 = objData.data[0];
        customerID            = soObj.entityid;

    if(soObj.email)
        {
          var filters = [];
              filters.push(new nlobjSearchFilter('email',null,'is',soObj.email));
          var columns = [];
              columns.push(new nlobjSearchColumn('internalid'));
              columns.push(new nlobjSearchColumn('email'));
          var results = nlapiSearchRecord('customer',null,filters,columns);
            if(results)
            {
              customerID = results[(results.length-1)].getValue('internalid');
            }
        }
        // end searching for a customer ID match 
    var transactionType = 'salesorder';
    if(settingsObj.getFieldValue('custrecord_pm_int_transform_sales_order') === '1')
    {
      transactionType = 'cashsale';
    }
    if(step)
    {
      transactionType = 'estimate';
    }


    var rec = nlapiCreateRecord(transactionType);

    rec.setFieldValue('entity', setValue(customerID)); // entity id is passed in by default.
    //rec.setFieldValue('customform',settingsObj.getFieldValue('custrecord_pm_int_sales_order_form'));
    rec.setFieldValue('leadsource',setValue(soObj.leadsource));
    // override the address block if nominated 
    if(soObj.usecustomeraddress)
    {
      if(soObj.usecustomeraddress == 'F')
      {
        // override the address with a custom address value 
        rec.setFieldValue('billaddress',setValue(soObj.billingaddress));
        rec.setFieldValue('shipaddress',setValue(soObj.shippingaddress));              
      }      
      else
      {
        rec.setFieldValue('billaddresslist',setValue(soObj.billaddresslist));
        rec.setFieldValue('shipaddresslist',setValue(soObj.shipaddresslist));              
      }
    }
    // otherwise use the default address nominated on the transaction 
    else
    {
      rec.setFieldValue('billaddresslist',setValue(soObj.billaddresslist));
      rec.setFieldValue('shipaddresslist',setValue(soObj.shipaddresslist));
    }
    // end applying it 

    rec.setFieldValue('location',setValue(soObj.location));
    rec.setFieldValue('currency',setValue(soObj.currency));
    rec.setFieldValue('class',setValue(soObj.class));
    rec.setFieldValue('department',setValue(soObj.department));
    rec.setFieldValue('salesrep',setValue(soObj.salesrep));
    rec.setFieldValue('shipmethod',setValue(soObj.shipmethod));

    // deduct tax from shipping items 
    if(settingsObj.getFieldValue('custrecord_pm_int_ded_tax_ship_item'))
    {
      if(settingsObj.getFieldValue('custrecord_pm_int_inbound_tax_rate') === '1')
      {
      soObj.shippingcost = exGSTAU(soObj.shippingcost);
      }
      else
      {
      soObj.shippingcost = exGSTNZ(parseFloat(soObj.shippingcost));
      }
    }
    // end deduct tax from shipping
    if(soObj.shippingcost)
    {
    rec.setFieldValue('shippingcost',soObj.shippingcost);      
    }

    // deduct tax from discount items 
    if(settingsObj.getFieldValue('custrecord_pm_int_ded_disc_tax'))
    {
        if(settingsObj.getFieldValue('custrecord_pm_int_inbound_tax_rate') === '1')
        {
        soObj.discountrate = exGSTAU(parseFloat(soObj.discountrate));      
        }
        else
        {
        soObj.discountrate = exGSTNZ(parseFloat(soObj.discountrate));       
        }
    }
    if(soObj.discountrate)
    {
      var discount       = soObj.discountrate;
      var discountAmount = discount.replace('--','-');
      //      var discount = soObj.discountrate;
      //    		discount = discount.replace('--','-');
      //rec.setFieldValue('discountrate',setValue(discount));
      rec.setFieldValue('discountrate',setValue(discountAmount));
      rec.setFieldValue('discountitem',setValue(soObj.discount));
    }
    // end discount rate adjustments 

    rec.setFieldValue('istaxable',setValue(soObj.istaxable));    
    rec.setFieldValue('paymentmethod',   	settingsObj.getFieldValue('custrecord_pm_int_payment_method'));
    // rec.setFieldValue('terms','4');  -- removed because its unknown why its actually here 
    var arrItems              = soObj.items;
    // SO Items


    for ( var i = 0; i < arrItems.length; i++)
    {
      dLog('Line Items', 'Item: ' + arrItems[i].internalid + ' Quantity ' + arrItems[i].quantity );
      rec.selectNewLineItem('item');
      rec.setCurrentLineItemValue('item', 'item', setValue(arrItems[i].internalid));
      rec.setCurrentLineItemValue('item', 'quantity', setValue(arrItems[i].quantity));
      var arrCustomCols = arrItems[i].custom_fields;
      for (var x in arrCustomCols)
      {
        rec.setCurrentLineItemValue('item', arrCustomCols[x].name, arrCustomCols[x].value);
      }
      rec.commitLineItem('item');
    }
      /* custom fields section */
      var arrCustomFields = soObj.custom_fields;		// custom_fields array on the suitelet.
      //var arrCustomFields = custFields[i].custom_fields;

      for ( var j = 0; j < arrCustomFields.length; j++)
      {
        rec.setFieldValue(arrCustomFields[j].name, arrCustomFields[j].value);
      }
      /* end custom fields section */
      rec.setFieldValue('custbody_pm_int_webstore',settingsObj.getFieldValue('id'));

      // record payment if it is a cash sale 
      if(transactionType === 'cashsale')
      {
        if(settingsObj.getFieldValue('custrecord_pm_rec_cash_sale_dep') === '1')
        {
          rec.setFieldValue('undepfunds','F');
          rec.setFieldValue('account',settingsObj.getFieldValue('custrecord_pm_int_cash_sale_acc'));          
        }
      }
      // end cash sale deposit payment 
      var salesorderID = nlapiSubmitRecord(rec, true, true);
      if(salesorderID)
      {
      dLog('Estimate ', salesorderID);
      logInboundRequests('1',settingsObj.getFieldValue('id'), 'Quote Internal ID \n' + salesorderID );                    
       var custEstimateAction = customEstimateAction('create',salesorderID);
      }
      objResponseData.internalid = salesorderID;

    /* automatically record a deposit payment if nominated */
    if(! step )
    {
    if(settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep') == '1' || settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep')  == '3' )
    {
        // if based on using Template 
        if(settingsObj.getFieldValue('custrecord_pm_int_tran_rec_dep')  == '3')
        {
            if(! soObj.recorddeposit)
            {
              return objResponseData;
            } 
            else 
            {
              if(soObj.recorddeposit == 'F')
              {
                  return objResponseData;
              }
            }
        }
        // end recording deposit process 

        var soId 	= salesorderID;
        var soRec 	= nlapiLoadRecord('salesorder',soId);
        var record 	= nlapiCreateRecord('customerdeposit', {entity:soRec.getFieldValue('entity'), salesorder:soId});
        if(soObj.deposit_customform)
          {
            record.setFieldValue('customform',        setValue(soObj.deposit_customform));          
          }
          else
          {
            record.setFieldValue('customform',        settingsObj.getFieldValue('custrecord_pm_int_dep_form'));
          }

          if(soObj.deposit_paymentmethod)
          {
            record.setFieldValue('paymentmethod',     setValue(soObj.deposit_paymentmethod));            
          }
          else 
          {
            record.setFieldValue('paymentmethod',     settingsObj.getFieldValue('custrecord_pm_int_dep_pay_meth'));            
          }

          if(soObj.deposit_location)
          {
            record.setFieldValue('location',          setValue(soObj.deposit_location));            
          }
          else 
          {
            record.setFieldValue('location',          settingsObj.getFieldValue('custrecord_pm_int_dep_location'));            
          }

          if(soObj.deposit_department)
          {
            record.setFieldValue('department',        setValue(soObj.deposit_department));
          }
          else 
          {
            record.setFieldValue('department',        settingsObj.getFieldValue('custrecord_pm_int_dep_depart'));            
          }

          if(soObj.deposit_class)
          {
            record.setFieldValue('class',             setValue(soObj.deposit_class));
          }
          else 
          {
            record.setFieldValue('class',             settingsObj.getFieldValue('custrecord_pm_int_deposit_class'));            
          }

          // cant change these
          record.setFieldValue('customer',			    customerID);
          record.setFieldValue('salesorder',			  salesorderID);
          // end cant chage these

          if(settingsObj.getFieldValue('custrecord_pm_int_partial_dep') === '1')
          {
            //setValue(soObj.grandTotal)
            if(soObj.deposit[0].payment)
            {
              record.setFieldValue('payment',           setValue(soObj.deposit[0].payment)); 
            }
            else 
            {
              record.setFieldValue('payment',           setValue(soObj.grandtotal));                          
            }
          }

          // if payment is via paypal, align it to the paypal account 
            if(settingsObj.getFieldValue('custrecord_pm_int_dep_acc'))
            {
              record.setFieldValue('undepfunds',			 'F');
                  if(soObj.paypal === 'true')
                  {
                  record.setFieldValue('account',				    settingsObj.getFieldValue('custrecord_pm_int_paypal_account'));                        
                  }
                  else 
                  {
                        // if account id is nominated, use it, otherwise default back to settings defined
                        if(soObj.deposit[0].account)
                        {
                          record.setFieldValue('account',   setValue(soObj.deposit[0].account) );                                
                        }
                        else 
                        {
                          record.setFieldValue('account',           settingsObj.getFieldValue('custrecord_pm_int_dep_acc'));
                        }
                  }
            }
            else 
            {
              record.setFieldValue('undepfunds',			 'T');            
            }
            //depositID 									      = nlapiSubmitRecord(record,true,true);
            //objResponseData.depositID	 				= depositID;
            //objResponseData.entityid 					= customerID;
    }
}
    /* // end recording a deposit payment if nominated */
    /* // end create cash sale conversion */
  }
  catch (e)
  {
    var stErrMsg = '';
    if (e.getDetails !== undefined)
    {
      stErrMsg = e.getCode() + ' ' + e.getDetails() + ' ' + e.getStackTrace();
      logInboundRequests('5',settingsObj.getFieldValue('id'), 'Quote - Error Occured - Undefined Error \n' +  stErrMsg);
    }
    else
    {
      stErrMsg = e.toString();
    }
    // log that the transaction failed 
    logInboundRequests('5',settingsObj.getFieldValue('id'), 'Quote - Error Occured \n' +  stErrMsg);      
    // end logging that the transaction failed 
    objResponseData.status              = "FAILED";
    objResponseData.responsemessage 	  = stErrMsg;
    objResponseData.responsecode 		    = "99";
    objResponseData.internalid 		      = '';
  }
  dLog('END', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CREATE ESTIMATE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  return objResponseData;
}
// end create sales order function




// create customer function
function createCustomer(objData,settingsObj)
{
  dLog('START', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CREATE CUSTOMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  // preparation of return object
    var objResponseData 				          = {};
    objResponseData.status 				        = "success";
    objResponseData.responsemessage 	    = "";
    objResponseData.responsecode 		      = "00";
    objResponseData.data 				          = '';
    objResponseData.internalid 		        = '';
    objResponseData.entityid 			        = '';
    // end preparation of return object
	var existingCustomerID                  = '';

  try
	{
      var customerObj                       = objData.data[0];
  		var entityID                          = '';
  		var record                            = nlapiCreateRecord('customer');
      var custDefaultBillingID              = '';
      var custDefaultShippingID             = '';
      var custRecord                        = '';
      var newContact                        = '';
      
  		/* prepare columns & filters */

			var columns = [];
			columns.push(new nlobjSearchColumn('entityid'));
			columns.push(new nlobjSearchColumn('internalid'));
			columns.push(new nlobjSearchColumn('firstname'));
			columns.push(new nlobjSearchColumn('lastname'));
			columns.push(new nlobjSearchColumn('companyname'));
			var filters = [];
			/* // end preparing columns & filters */
			if (customerObj.isperson == 'T')
			{
				/* prepare search filter object */
				filters.push(new nlobjSearchFilter('firstname',null,'is',customerObj.firstname));
				filters.push(new nlobjSearchFilter('lastname',null,'is',customerObj.lastname));
				entityID = customerObj.firstname + ' ' + customerObj.lastname;
				if(customerObj.entityid !== '')
				{
          if(customerObj.entityid !== 'undefined')
          {
					entityID = entityID + ' ' + customerObj.entityid;            
          }
				}
				/* // end prepare search filter object */
			}
			else
			{
				/* prepare companyname filter object */
				filters.push(new nlobjSearchFilter('companyname',null,'is',customerObj.customername));
				entityID = customerObj.customername;
				if(customerObj.entityid !== '')
				{
					entityID = entityID;
				}
				/* // end prepare companyname filter object */
			}

      var entityString = '';
        if(settingsObj.getFieldValue('custrecord_pm_int_cust_ent_pref'))
          { 
          entityString += settingsObj.getFieldValue('custrecord_pm_int_cust_ent_pref');
          }
          entityString += entityID;

        if(settingsObj.getFieldValue('custrecord_pm_int_cust_ent_suf'))
          { 
          entityString += settingsObj.getFieldValue('custrecord_pm_int_cust_ent_suf');
          }

          entityID = entityString;

			// perform search to validat customer record
			var customerExists = nlapiSearchRecord('customer',null,filters,columns);

			/* // end performing search for entity id as company name */
			if(customerExists)
			{
				entityID = entityID + ' ' + (customerExists.length + 1);
			}
			// set the entity id		
			// end  update for auto processing of entityid

		  //var entityId  = customerObj.entityid;
 		  var entityId = entityID;

			var col = [];
			col[0] = new nlobjSearchColumn('internalId');
			var filter = [];
			filter.push(new nlobjSearchFilter('entityid',null,'is',entityId));
			var searchresults = nlapiSearchRecord('customer', null, filter, col);

				if(searchresults)
				{
					existingCustomerID = searchresults[0].id;
          // load the customers addressbook to get the internal IDs for each address type
              custRecord = nlapiLoadRecord('customer',existingCustomerID);
              // define the record as created by Connect, so it can be used by User Event Records
              custRecord.setFieldValue('custentity_pm_int_created_via_connect','1');
              custDefaultBillingID  = '';
              custDefaultShippingID = '';
          for(i=1;i<=custRecord.getLineItemCount('addressbook');i++)
          {
              if(custRecord.getLineItemValue('addressbook','defaultshipping',i) === 'T')
              {
                  custDefaultShippingID = custRecord.getLineItemValue('addressbook','internalid',i);
              }
              if(custRecord.getLineItemValue('addressbook','defaultbilling',i) === 'T')
              {
                  custDefaultBillingID = custRecord.getLineItemValue('addressbook','internalid',i);
              }
          }  
          // end loading the customers addressbook
				}
		/* end checking the customer entity id */

		// Customer Fields
		if (customerObj.isperson == 'T')
		{
			record.setFieldValue('isperson', 'T');
			record.setFieldValue('salutation', setValue(customerObj.salutation));
			record.setFieldValue('firstname', setValue(customerObj.firstname));
			record.setFieldValue('middlename', setValue(customerObj.middlename));
			record.setFieldValue('lastname', setValue(customerObj.lastname));
			record.setFieldValue('mobilephone', setValue(customerObj.mobilephone));
			record.setFieldValue('homephone', setValue(customerObj.homephone));   
  	  record.setFieldValue('entityid', setValue(	entityID ));         
		}
		else
		{
			record.setFieldValue('isperson', 'F');
      record.setFieldValue('entityid', setValue(	entityID ));         
		}
    record.setFieldValue('custentity_pm_int_update_ws_pass','F');
		// end contact record
		record.setFieldValue('companyname', setValue(customerObj.customername));
		record.setFieldValue('parent', setValue(customerObj.parent));
		record.setFieldValue('phone', setValue(customerObj.phone));
		record.setFieldValue('fax', setValue(customerObj.fax));
		record.setFieldValue('email', setValue(customerObj.email));
		//record.setFieldValue('entityid', setValue(customerObj.entityid));
		record.setFieldValue('currency', setValue(customerObj.currency));
    // specifically nominate the integration they are coming in on

    record.setFieldValue('custentity_pm_int_sel_webstore',settingsObj.getFieldValue('id'));

    // end defining the integration they are coming in on

		var arrAddress = customerObj.addressbook;

		for (var x in arrAddress)
		{
      var addNewAddress = true;   // default process is to create a new address;
      // check to see if address exists or not 
      if(existingCustomerID !== '')
      {
        var addressCheck = checkCustomerAddressbookAddress(arrAddress[x],existingCustomerID);
        if(addressCheck.exists === true)
        {
          addNewAddress = false;
        }        
      }

      
      if(addNewAddress === true)
        {
      			// Address
      			record.selectNewLineItem('addressbook');
      			if (arrAddress[x].defaultshipping == 'T')
      				record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
      			if (arrAddress[x].defaultbilling == 'T')
      				record.setCurrentLineItemValue('addressbook', 'defaultbilling', 'T');
      			if (arrAddress[x].is_residential == 'T')
      				record.setCurrentLineItemValue('addressbook', 'isresidential', 'T');

        			record.setCurrentLineItemValue('addressbook', 'label', setValue(arrAddress[x].label));
        			record.setCurrentLineItemValue('addressbook', 'attention', setValue(arrAddress[x].attention));
        			record.setCurrentLineItemValue('addressbook', 'addressee', setValue(arrAddress[x].addressee));
        			record.setCurrentLineItemValue('addressbook', 'phone', setValue(arrAddress[x].phone));
        			record.setCurrentLineItemValue('addressbook', 'addr1', setValue(arrAddress[x].addr1));
        			record.setCurrentLineItemValue('addressbook', 'addr2', setValue(arrAddress[x].addr2));
        			record.setCurrentLineItemValue('addressbook', 'addr3', setValue(arrAddress[x].addr3));
        			record.setCurrentLineItemValue('addressbook', 'city', setValue(arrAddress[x].city));
        			record.setCurrentLineItemValue('addressbook', 'state', setValue(arrAddress[x].state));
        			record.setCurrentLineItemValue('addressbook', 'zip', setValue(arrAddress[x].zip));
        			record.setCurrentLineItemValue('addressbook', 'country', setValue(arrAddress[x].country));

        			if (arrAddress[x].override == 'T')
        			{
        				record.setCurrentLineItemValue('addressbook', 'override', 'T');
        				record.setCurrentLineItemValue('addressbook', 'addrtext', setValue(arrAddress[x].addr_text));
        			}
  			record.commitLineItem('addressbook');
        }  
		}
			/* custom fields section */
			var arrCustomFields = customerObj.custom_fields;
			for ( var j = 0; j < arrCustomFields.length; j++)
			{
				record.setFieldValue(arrCustomFields[j].name, arrCustomFields[j].value);
			}
			/* end custom fields section */
			var customerID = nlapiSubmitRecord(record, true, true);
      if(customerID)
      {
      dLog('Customer Created ID',customerID);
      logInboundRequests('1',settingsObj.getFieldValue('id'), 'Customer - Internal ID \n' + customerID );                    
      var custCustomerAction             = customCustomerAction('create',customerID);               
      }

			if(customerObj.isperson == 'F')
			{
				  var contactRecord = nlapiCreateRecord('contact');
              contactRecord.setFieldValue('custentity_pm_int_created_via_connect','1');
							contactRecord.setFieldValue('company', setValue(customerID));
							contactRecord.setFieldValue('salutation', setValue(customerObj.salutation));
							contactRecord.setFieldValue('firstname', setValue(customerObj.firstname));
							contactRecord.setFieldValue('middlename', setValue(customerObj.middlename));
							contactRecord.setFieldValue('lastname', setValue(customerObj.lastname));
							contactRecord.setFieldValue('mobilephone', setValue(customerObj.mobilephone));
              contactRecord.setFieldValue('phone', setValue(customerObj.phone));
              contactRecord.setFieldValue('homephone', setValue(customerObj.homephone));
              contactRecord.setFieldValue('email',setValue(customerObj.email));              
              contactRecord.setFieldValue('fax',setValue(customerObj.fax));
              // specifically mark which integration the customer is being created from 
              contactRecord.setFieldValue('custentity_pm_int_sel_webstore',settingsObj.getFieldValue('id'));          
							for (var y in arrAddress)
							{
								// Address
								contactRecord.selectNewLineItem('addressbook');
								if (arrAddress[y].defaultshipping == 'T')
									contactRecord.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
								if (arrAddress[y].defaultbilling == 'T')
									contactRecord.setCurrentLineItemValue('addressbook', 'defaultbilling', 'T');
								if (arrAddress[y].isresidential == 'T')
									contactRecord.setCurrentLineItemValue('addressbook', 'isresidential', 'T');

								contactRecord.setCurrentLineItemValue('addressbook', 'label', setValue(arrAddress[y].label));
								contactRecord.setCurrentLineItemValue('addressbook', 'attention', setValue(arrAddress[y].attention));
								contactRecord.setCurrentLineItemValue('addressbook', 'addressee', setValue(arrAddress[y].addressee));
								contactRecord.setCurrentLineItemValue('addressbook', 'phone', setValue(arrAddress[y].phone));
								contactRecord.setCurrentLineItemValue('addressbook', 'addr1', setValue(arrAddress[y].addr1));
								contactRecord.setCurrentLineItemValue('addressbook', 'addr2', setValue(arrAddress[y].addr2));
								contactRecord.setCurrentLineItemValue('addressbook', 'addr3', setValue(arrAddress[y].addr3));
								contactRecord.setCurrentLineItemValue('addressbook', 'city', setValue(arrAddress[y].city));
								contactRecord.setCurrentLineItemValue('addressbook', 'state', setValue(arrAddress[y].state));
								contactRecord.setCurrentLineItemValue('addressbook', 'zip', setValue(arrAddress[y].zip));
								contactRecord.setCurrentLineItemValue('addressbook', 'country', setValue(arrAddress[y].country));

								if (arrAddress[y].override == 'T')
								{
									contactRecord.setCurrentLineItemValue('addressbook', 'override', 'T');
									contactRecord.setCurrentLineItemValue('addressbook', 'addrtext', setValue(arrAddress[y].addr_text));
								}
								contactRecord.commitLineItem('addressbook');
							}
							// end contact record address
							newContact = nlapiSubmitRecord(contactRecord,true,true);
              if(newContact)
              {
              dLog('Contact Created ID',newContact);
              logInboundRequests('1',settingsObj.getFieldValue('id'), 'Contact - Internal ID \n' + newContact );                    
              var custContactAction = customContactAction('create',newContact);                
              }
						}
						// end contact record creation          
			objResponseData.internalid 			   = customerID;
			objResponseData.entityid 				   = entityID;
      objResponseData.contactid          = newContact;
	}
	catch (e)
	{
		var stErrMsg = '';
		if (e.getDetails !== undefined)
		{
			stErrMsg = e.getCode() + ' ' + e.getDetails() + ' ' + e.getStackTrace();
      logInboundRequests('5',settingsObj.getFieldValue('id'), 'Error Occured - Undefined Error \n' +  stErrMsg);
      dLog('Error', stErrMsg);
    }
		else
		{
			stErrMsg = e.toString();
		}
    // prepare return error response
		objResponseData.status 				        = "failed";
		objResponseData.responsemessage 	    = stErrMsg;
		objResponseData.responsecode 		      = "99";
		objResponseData.internalid 		        = existingCustomerID;
		objResponseData.entityid 			        = '';
    objResponseData.defaultbilling        = '';
    objResponseData.defaultshipping       = '';
    logInboundRequests('5',settingsObj.getFieldValue('id'), 'Error Occured \n' +  stErrMsg );      
    dLog('Error', 'Error Occured \n' +  stErrMsg );
    // end prepare return error response
	}
  dLog('END', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CREATE CUSTOMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
	return objResponseData;
}
// end creating a customer function











// create customer function
function updateCustomer(objData,settingsObj,customerID)
{
  dLog('START', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UPDATE CUSTOMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  dLog('Customer ID', customerID);
  // preparation of return object
    var objResponseData 				          = {};
    objResponseData.status 				        = "success";
    objResponseData.responsemessage 	    = "";
    objResponseData.responsecode 		      = "00";
    objResponseData.data 				          = '';
    objResponseData.internalid 		        = '';
    objResponseData.entityid 			        = '';
    // end preparation of return object
	var existingCustomerID                  = '';
  if(customerID)
  {
    existingCustomerID                    = customerID;
  }
	//dLog('cust data', JSON.stringify(objData.data[0]));
  try
	{
    var customerObj                       = objData.data[0];
		var entityID                          = '';
		var record                            = nlapiLoadRecord('customer',customerID);
    var custDefaultBillingID  = '';
    var custDefaultShippingID = '';
    var custRecord = '';
    var newContact = '';
		/* end checking the customer entity id */
		// Customer Fields
		if (customerObj.isperson == 'T')
		{
			record.setFieldValue('isperson', 'T');
			record.setFieldValue('salutation', setValue(customerObj.salutation));
			record.setFieldValue('firstname', setValue(customerObj.firstname));
			record.setFieldValue('middlename', setValue(customerObj.middlename));
			record.setFieldValue('lastname', setValue(customerObj.lastname));
			record.setFieldValue('mobilephone', setValue(customerObj.mobilephone));
			record.setFieldValue('homephone', setValue(customerObj.homephone));      
		}
		else
		{
			record.setFieldValue('isperson', 'F');
		}
    record.setFieldValue('custentity_pm_int_update_ws_pass','F');
		// end contact record
		record.setFieldValue('companyname', setValue(customerObj.customername));
		record.setFieldValue('parent', setValue(customerObj.parent));
		record.setFieldValue('phone', setValue(customerObj.phone));
		record.setFieldValue('fax', setValue(customerObj.fax));
		record.setFieldValue('email', setValue(customerObj.email));
		//record.setFieldValue('entityid', setValue(customerObj.entityid));
		record.setFieldValue('currency', setValue(customerObj.currency));
    // specifically nominate the integration they are coming in on
    record.setFieldValue('custentity_pm_int_sel_webstore',settingsObj.getFieldValue('id'));
    // end defining the integration they are coming in on

		var arrAddress = customerObj.addressbook;
		for (var x in arrAddress)
		{
      var addNewAddress = true;   // default process is to create a new address;
      // check to see if address exists or not 
      if(existingCustomerID !== '')
      {
        var addressCheck = checkCustomerAddressbookAddress(arrAddress[x],existingCustomerID);
        if(addressCheck.exists === true)
        {
          addNewAddress = false;
          var numberOfAddresses = record.getLineItemCount('addressbook');
          for (var i=1; i <= numberOfAddresses; i++) 
          {
            var internalid = record.getLineItemValue('addressbook','internalid',i);
            if(internalid == addressCheck.internalid)
            {
              record.selectLineItem('addressbook',i);
              if (arrAddress[x].defaultshipping == 'T')
              {
          				record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
              }

              if (arrAddress[x].defaultbilling == 'T')
              {
                record.setCurrentLineItemValue('addressbook', 'defaultbilling', 'T');
              }
              record.commitLineItem('addressbook'); 
            }
          }
        }        
      }
      if(addNewAddress === true)
        {
      			// Address
      			record.selectNewLineItem('addressbook');
      			if (arrAddress[x].defaultshipping == 'T')
      				record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
      			if (arrAddress[x].defaultbilling == 'T')
      				record.setCurrentLineItemValue('addressbook', 'defaultbilling', 'T');
      			if (arrAddress[x].is_residential == 'T')
      				record.setCurrentLineItemValue('addressbook', 'isresidential', 'T');
        			record.setCurrentLineItemValue('addressbook', 'label', setValue(arrAddress[x].label));
        			record.setCurrentLineItemValue('addressbook', 'attention', setValue(arrAddress[x].attention));
        			record.setCurrentLineItemValue('addressbook', 'addressee', setValue(arrAddress[x].addressee));
        			record.setCurrentLineItemValue('addressbook', 'phone', setValue(arrAddress[x].phone));
        			record.setCurrentLineItemValue('addressbook', 'addr1', setValue(arrAddress[x].addr1));
        			record.setCurrentLineItemValue('addressbook', 'addr2', setValue(arrAddress[x].addr2));
        			record.setCurrentLineItemValue('addressbook', 'addr3', setValue(arrAddress[x].addr3));
        			record.setCurrentLineItemValue('addressbook', 'city', setValue(arrAddress[x].city));
        			record.setCurrentLineItemValue('addressbook', 'state', setValue(arrAddress[x].state));
        			record.setCurrentLineItemValue('addressbook', 'zip', setValue(arrAddress[x].zip));
        			record.setCurrentLineItemValue('addressbook', 'country', setValue(arrAddress[x].country));
        			if (arrAddress[x].override == 'T')
        			{
        				record.setCurrentLineItemValue('addressbook', 'override', 'T');
        				record.setCurrentLineItemValue('addressbook', 'addrtext', setValue(arrAddress[x].addr_text));
        			}
        			record.commitLineItem('addressbook');
          }
          
		}
			/* custom fields section */
			var arrCustomFields = customerObj.custom_fields;
			for ( var j = 0; j < arrCustomFields.length; j++)
			{
				record.setFieldValue(arrCustomFields[j].name, arrCustomFields[j].value);
			}
			/* end custom fields section */      
			customerID = nlapiSubmitRecord(record, true, true);
      dLog('customerSbubmitted','chicken dance');
			if(customerObj.isperson == 'F')
			{
				  var contactRecord = nlapiCreateRecord('contact');
              contactRecord.setFieldValue('custentity_pm_int_created_via_connect','1');
							contactRecord.setFieldValue('company', setValue(customerID));
							contactRecord.setFieldValue('salutation', setValue(customerObj.salutation));
							contactRecord.setFieldValue('firstname', setValue(customerObj.firstname));
							contactRecord.setFieldValue('middlename', setValue(customerObj.middlename));
							contactRecord.setFieldValue('lastname', setValue(customerObj.lastname));
							contactRecord.setFieldValue('mobilephone', setValue(customerObj.mobilephone));
              contactRecord.setFieldValue('phone', setValue(customerObj.phone));
              contactRecord.setFieldValue('homephone', setValue(customerObj.homephone));
              contactRecord.setFieldValue('email',setValue(customerObj.email));              
              contactRecord.setFieldValue('fax',setValue(customerObj.fax));
              // specifically mark which integration the customer is being created from 
              contactRecord.setFieldValue('custentity_pm_int_sel_webstore',settingsObj.getFieldValue('id'));          
							for (var y in arrAddress)
							{
								// Address
								contactRecord.selectNewLineItem('addressbook');
								if (arrAddress[y].defaultshipping == 'T')
									contactRecord.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
								if (arrAddress[y].defaultbilling == 'T')
									contactRecord.setCurrentLineItemValue('addressbook', 'defaultbilling', 'T');
								if (arrAddress[y].isresidential == 'T')
									contactRecord.setCurrentLineItemValue('addressbook', 'isresidential', 'T');

								contactRecord.setCurrentLineItemValue('addressbook', 'label', setValue(arrAddress[y].label));
								contactRecord.setCurrentLineItemValue('addressbook', 'attention', setValue(arrAddress[y].attention));
								contactRecord.setCurrentLineItemValue('addressbook', 'addressee', setValue(arrAddress[y].addressee));
								contactRecord.setCurrentLineItemValue('addressbook', 'phone', setValue(arrAddress[y].phone));
								contactRecord.setCurrentLineItemValue('addressbook', 'addr1', setValue(arrAddress[y].addr1));
								contactRecord.setCurrentLineItemValue('addressbook', 'addr2', setValue(arrAddress[y].addr2));
								contactRecord.setCurrentLineItemValue('addressbook', 'addr3', setValue(arrAddress[y].addr3));
								contactRecord.setCurrentLineItemValue('addressbook', 'city', setValue(arrAddress[y].city));
								contactRecord.setCurrentLineItemValue('addressbook', 'state', setValue(arrAddress[y].state));
								contactRecord.setCurrentLineItemValue('addressbook', 'zip', setValue(arrAddress[y].zip));
								contactRecord.setCurrentLineItemValue('addressbook', 'country', setValue(arrAddress[y].country));

								if (arrAddress[y].override == 'T')
								{
									contactRecord.setCurrentLineItemValue('addressbook', 'override', 'T');
									contactRecord.setCurrentLineItemValue('addressbook', 'addrtext', setValue(arrAddress[y].addr_text));
								}
								contactRecord.commitLineItem('addressbook');
							}
							// end contact record address                          
							newContact = nlapiSubmitRecord(contactRecord,true,true);            
						}
						// end contact record creation          
			objResponseData.internalid 			   = customerID;
			objResponseData.entityid 				   = entityID;
      objResponseData.contactid          = newContact;
	}
	catch (e)
	{
		var stErrMsg = '';
		if (e.getDetails !== undefined)
		{
			stErrMsg = e.getCode() + ' ' + e.getDetails() + ' ' + e.getStackTrace();
      logInboundRequests('5',settingsObj.getFieldValue('id'), 'Customer Update - Error Occured - Undefined Error \n' +  stErrMsg );
		}
		else
		{
			stErrMsg = e.toString();
		}
    // prepare return error response
		objResponseData.status 				        = "failed";
		objResponseData.responsemessage 	    = stErrMsg;
		objResponseData.responsecode 		      = "99";
		objResponseData.internalid 		        = existingCustomerID;
		objResponseData.entityid 			        = '';
    objResponseData.defaultbilling        = '';
    objResponseData.defaultshipping       = '';
    logInboundRequests('5',settingsObj.getFieldValue('id'), 'Customer Update - Error Occured \n' +  stErrMsg);      
    // end prepare return error response
	}
  dLog('END', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UPDATE CUSTOMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
	return objResponseData;
}
// end creating a customer function













function getAddressbook(recordType,internalid)
{
  // load the customers addressbook to get the internal IDs for each address type
  var record = nlapiLoadRecord(recordType,internalid);
  var addressID = {};
  for(i=1;i<=record.getLineItemCount('addressbook');i++)
  {
      if(record.getLineItemValue('addressbook','defaultshipping',i) === 'T')
      {
          addressID.custDefaultShippingID = record.getLineItemValue('addressbook','internalid',i);
      }
      if(record.getLineItemValue('addressbook','defaultbilling',i) === 'T')
      {
          addressID.custDefaultBillingID = record.getLineItemValue('addressbook','internalid',i);
      }
  }  
  // end loading the customers addressbook  
  return addressID;
}






function payInvoice(request)
{
  dLog('START', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PAY INVOICE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  var record = nlapiCreateRecord('customerpayment');
  record.setFieldValue('invoice', request.getParameter('invoiceID'));
  record.setFieldValue('customer',request.getParameter('customerID'));
  record.setFieldValue('department',request.getParameter('department'));
  record.setFieldValue('payment',request.getParameter('payment'));
  var payment = nlapiSubmitRecord(record);
  var returnObj = {};
      returnObj.status = "success";
  if(returnObj.status == "success")
  {
    dLog('Invoice', request.getParameter('invoiceID'));
    dLog('Customer',request.getParameter('customerID'));
    dLog('Department',request.getParameter('department'));
    dLog('Payment',request.getParameter('payment'));
    dLog('Payment ID',payment);
  }
  dLog('END', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PAY INVOICE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  return returnObj;
}



// create a deposit function
function createDeposit(request)
{
  // preparation of return object
  var objResponseData 				          	= {};
  objResponseData.status 				        = "success";
  objResponseData.responsemessage 	    		= "";
  objResponseData.responsecode 		      		= "00";
  objResponseData.data 				          	= '';
  objResponseData.internalid 		        	= '';
  objResponseData.entityid 			        	= '';
  // end preparation of return object
  
  try
	{
		//var depositData  = objData.data[0];
		var soId 		     = request.getParameter('salesOrderID');
		var soRec 		   = nlapiLoadRecord(request.getParameter('type'),soId);
		var record 		   = nlapiCreateRecord('customerdeposit', {recordmode:'dynamic', entity:soRec.getFieldValue('entity'), salesorder:soId});
			record.setFieldValue('customer',		request.getParameter('customerID'));
			record.setFieldValue('salesorder',		request.getParameter('salesOrderID'));
			record.setFieldValue('payment',			request.getParameter('payment'));
			record.setFieldValue('trandate',        request.getParameter('trandate'));
		  record.setFieldValue('paymentmethod',   request.getParameter('paymentMethod'));
			record.setFieldValue('undepfunds',		'F');
			record.setFieldValue('account',			request.getParameter('account'));

			var arrCustomFields = depositData.custom_fields;
			for ( var j = 0; j < arrCustomFields.length; j++)
			{
				record.setFieldValue(arrCustomFields[j].name, arrCustomFields[j].value);
			}
	    	depositID = nlapiSubmitRecord(record,true);
			  objResponseData.internalid 			= depositID;
	}
	catch (e)
	{
		var stErrMsg = '';
		if (e.getDetails !== undefined)
		{
			stErrMsg = e.getCode() + ' ' + e.getDetails() + ' ' + e.getStackTrace();
		}
		else
		{
			stErrMsg = e.toString();
		}
		objResponseData.status 				     = "FAILED";
		objResponseData.responsemessage 	 = stErrMsg;
		objResponseData.responsecode 		   = "99";
		objResponseData.internalid 		     = "";
		objResponseData.entityid 			     = '';
	}
		return objResponseData;
}
// end create deposit function



// create cash sale function
function createCashSale(objData)
{
  try
  {
    var soObj = objData.data[0];
    var rec = nlapiCreateRecord('cashsale');
    rec.setFieldValue('entity', setValue(soObj.entityid)); // entity id is passed in by default.
    var arrItems = soObj.items;
    // SO Items
    for ( var i = 0; i < arrItems.length; i++)
    {
      rec.selectNewLineItem('item');
      rec.setCurrentLineItemValue('item', 'item', setValue(arrItems[i].internalid));
      rec.setCurrentLineItemValue('item', 'quantity', setValue(arrItems[i].quantity));
      var arrCustomCols = arrItems[i].custom_fields;
      for (var x in arrCustomCols)
      {
        rec.setCurrentLineItemValue('item', arrCustomCols[x].name, arrCustomCols[x].value);
      }
      rec.commitLineItem('item');
    }
      /* custom fields section */
      var arrCustomFields = soObj.custom_fields;		// custom_fields array on the suitelet.
      for ( var j = 0; j < arrCustomFields.length; j++)
      {
        rec.setFieldValue(arrCustomFields[j].name, arrCustomFields[j].value);
      }
      /* end custom fields section */

      var salesorderID = nlapiSubmitRecord(rec, true, true);
      objResponseData.internalid = salesorderID;

      /* automatically record a deposit payment if nominated */
      if(soObj.recordDeposit)
      {
        if (soObj.recordDeposit == 'T')
        {
          var soId 	    = salesorderID;
          var soRec 	  = nlapiLoadRecord('salesorder',soId);
          var record 	  = nlapiCreateRecord('customerdeposit', {entity:soRec.getFieldValue('entity'), salesorder:soId});
            record.setFieldValue('customer',			soObj.entityid);
            record.setFieldValue('salesorder',			salesorderID);
            record.setFieldValue('payment',				soObj.payment);
            record.setFieldValue('trandate',        	soObj.tranDate);
            record.setFieldValue('paymentmethod',   	soObj.paymentMethod);
            record.setFieldValue('undepfunds',			'F');
            record.setFieldValue('account',				soObj.account);
            depositID 									      = nlapiSubmitRecord(record,true,true);
            objResponseData.depositID	 				= depositID;
            objResponseData.entityid 					= soObj.entityid;
            objResponseData.ns_url 						= 'https://system.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soId + '&e=T&whence=';
        }
    }
    /* // end recording a deposit payment if nominated */
    /* todo, create cash sale conversion */
    /* // end create cash sale conversion */
  }
  catch (e)
  {
    var stErrMsg = '';
    if (e.getDetails !== undefined)
    {
      stErrMsg = e.getCode() + ' ' + e.getDetails() + ' ' + e.getStackTrace();
    }
    else
    {
      stErrMsg = e.toString();
    }

    objResponseData.status             = "failed";
    objResponseData.responsemessage 	 = stErrMsg;
    objResponseData.responsecode 		   = "99";
    objResponseData.internalid 		     = '';
  }
  return objResponseData;
}






function getWebsiteItems()
{
  var results = nlapiSearchRecord(null,'customsearch_pm_int_ws_item_list');
    if(results)
    {
      return results;
    }
}
// end function




function checkItems(settingsObj,itemcode)
{
  dLog('START', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CHECK ITEM >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');  
  var returnObj = {};
      returnObj.status = '0';
  if(itemcode)
  {
    var filters = [];

    	switch(settingsObj.getFieldValue('custrecord_pm_int_item_identifier'))
          {
            case '1':
				filters.push(new nlobjSearchFilter('internalid',null,'is',itemcode));
            break;

            case '2':
				filters.push(new nlobjSearchFilter('itemid',null,'is',itemcode));
            break;

            case '3':
				filters.push(new nlobjSearchFilter('upccode',null,'is',itemcode));
            break;
          }

    var columns = [];
        columns.push(new nlobjSearchColumn('internalid'));
    var results = nlapiSearchRecord('item',null,filters,columns);
    if(results)
    {
      returnObj.status = '1';
      returnObj.internalid = results[(results.length-1)].getValue('internalid');
      dLog('Item ',itemcode + '  found');
    }
    else 
    {
      dLog('Item ',itemcode + '  not found');      
    }
  }
  dLog('END', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CHECK ITEM >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');  
  return returnObj;
}
// end function


function checkCustomer(customerEmail,settingsObj,customerName,externalID,step)
{
    dLog('START', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CHECK CUSTOMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');  
    var filters = [];
    var searchTerm = '';
    if(step)
    {
      filters.push(new nlobjSearchFilter('custentity_pm_int_cust_con_id',null,'is',externalID).setOr(true));
      filters.push(new nlobjSearchFilter('externalid',null,'is',externalID));      
      searchTerm = externalID;
    }
    else 
    {
        switch(settingsObj.getFieldValue('custrecord_pm_identify_customers_by'))
        {
          case '1' : 
        	filters.push(new nlobjSearchFilter('companyname',null,'is',customerName));
          searchTerm = customerName;
          break;

          case '3' : 
          filters.push(new nlobjSearchFilter('companyname',null,'is',customerName).setOr(true));
          filters.push(new nlobjSearchFilter('email',null,'is',customerEmail));      
          searchTerm = customerName + ' ' + customerEmail;
          break;

          case '4' : 
    		    filters.push(new nlobjSearchFilter('custentity_pm_int_cust_con_id',null,'is',externalID));
            searchTerm = externalID;
          break;

          default :
          	filters.push(new nlobjSearchFilter('email',null,'is',customerEmail));
            searchTerm = customerEmail;
          break;
        }
    }

    var columns = [];
        columns.push(new nlobjSearchColumn('internalid'));
        columns.push(new nlobjSearchColumn('isinactive'));
        columns.push(new nlobjSearchColumn('isperson'));
    var results = nlapiSearchRecord('customer',null,filters,columns);
    if(results)
    {
      dLog('Customer', searchTerm + ' '  + ' found');
      return results[(results.length-1)].getValue('internalid');
    }
    else 
    {
            dLog('Customer', searchTerm +  'not found');  
    }
    dLog('END', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CHECK CUSTOMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');  
}
// end function


function checkCustomerAddressbookAddress(addressbook,customerID)
{
  try{
      var filters = [];
          filters.push(new nlobjSearchFilter('internalid',null,'is',customerID));
      var results = nlapiSearchRecord(null,'customsearch_pm_int_cust_addressbook_lis',filters);
      
      var returnObj = {};
          returnObj.exists = false;
          returnObj.internalid = '';
      var checkAddress = {};
          checkAddress.addressee = addressbook.addressee;
          checkAddress.attention = addressbook.attention;
          checkAddress.address1  = addressbook.addr1;
          checkAddress.address2  = addressbook.addr2;
          checkAddress.address3  = addressbook.addr3;
          checkAddress.city      = addressbook.city;
          checkAddress.state     = addressbook.state;
          checkAddress.country   = addressbook.country;
          checkAddress.zip       = addressbook.zip;

      if(results)
      {
        var addressbookObj = JSON.parse(JSON.stringify(results));
        for(i=0;i<=(addressbookObj.length-1);i++)
        {
          var addrline2 = '';
          if(addressbookObj[i].columns.address2)
          {
            addrline2 = addressbookObj[i].columns.address2;
          }

          var addressString1 = checkAddress.addressee + ' ' + checkAddress.attention + ' ' +  checkAddress.address1 + ' ' + checkAddress.address2 + ' ' + checkAddress.city + ' ' + checkAddress.state + ' ' + checkAddress.country + ' ' + checkAddress.zip;
          var addressString2 = addressbookObj[i].columns.addressee + ' ' + addressbookObj[i].columns.attention + ' ' +  addressbookObj[i].columns.address1 + ' ' + addrline2 + ' ' + addressbookObj[i].columns.city  + ' ' + addressbookObj[i].columns.state.name  + ' ' + addressbookObj[i].columns.country.internalid + ' ' + addressbookObj[i].columns.zipcode;
          //logInboundRequests('1',settingsObj.getFieldValue('id'), addressString1 + ' \n' + addressString2 + '\n' + matchString(addressString1 , addressString2) );
          if(matchString(addressString1 , addressString2))
          {
            //dLog('matchString',matchString(addressString1 , addressString2) + ' ' + addressbookObj[i].columns.internalid.internalid);
            returnObj.exists            = true;
            returnObj.internalid        = addressbookObj[i].columns.addressinternalid;
            returnObj.address1          = matchString(checkAddress.address1  , addressbookObj[i].columns.address1);
            returnObj.address2          = matchString(checkAddress.address2  , addressbookObj[i].columns.address2);
            returnObj.city              = matchString(checkAddress.city  , addressbookObj[i].columns.city);
            returnObj.state             = matchString(checkAddress.state  , addressbookObj[i].columns.state);
            returnObj.country           = matchString(checkAddress.country  , addressbookObj[i].columns.country);
            returnObj.zipCode           = matchString(checkAddress.zip , addressbookObj[i].columns.zipcode);
          }
        }
      }
      return returnObj;
  }
  catch(e)
  {
    dLog('error alert',e.message);
  }
}


function matchString(str1,str2)
{
  var returnString = false;
  if(str1 == str2)
  {
    returnString = true;
  }
  return returnString;
}
// end function


function checkIfEstimate(connectID)
{
  dLog('START', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CHECK IF ESTIMATE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  /*
  @NOTE - custom function to search NS to determine if a transaction is already defined as a quote
        - This will validate on the connect ID used on the transaction if defined
        - Created to stop the whinging from Icon By Design.
  */
  var returnObj = {};
      returnObj.status = false;
      returnObj.internalid = '';
  var filters   = [];
      filters.push(new nlobjSearchFilter('custbody_pm_int_tran_con_id',null,'is',connectID));
      filters.push(new nlobjSearchFilter('mainline',null,'is','T'));
      filters.push(new nlobjSearchFilter('recordtype',null,'is','estimate'));
  var columns = [];
      columns.push(new nlobjSearchColumn('internalid'));
      columns.push(new nlobjSearchColumn('type'));
  var results = nlapiSearchRecord('transaction',null,filters,columns);
  if(results)
  {
    dLog('STATUS', 'TRUE');
    returnObj.status = true;
    returnObj.internalid = results[(results.length-1)].getValue('internalid');
  }  
  else 
  {
  dLog('STATUS', 'FALSE');
  }
  dLog('END', '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CHECK IF ESTIMATE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  return returnObj;
}