/**
  * @desc : I am the template parsing library for the connect integration platform
  * @desc : I am a series of functions that fetch and parse data as required for integration purposes
  * @desc : Date Created : 2017-01-22
  * @desc : Version 1.0
  * @desc : Created by David Imrie
  * @function parseIntegrationTemplate
  * @function getIntegrationTemplateFile
  * @function getIntegrationTemplate
*/

function parseIntegrationTemplate(record,templateFile,miscObjects)
{
  /**
    * @desc   I obtain a template and parse it against the HandlebarsJS framework to prepare the required file for integration
    * @param  record        = the actual record being included in the integration request
    * @param  templateFile  = the templateFile being parsed for the webservice request
    * @param  miscObjects   = typically an array of data that is to be joined to the main data object
    * @return I return the parsed file ready for being posted to the external service
    * @return I allow NS data to be accessible by data.record.<> or data.childRecords[i].name notation
  **/
  var data              = {};
      data.childRecords = [];
      data.record       = record;
      /* pass in any additional data objects to be processed */
      if(miscObjects)
      {
            data.childRecords = miscObjects;
      }
      data = JSON.parse(JSON.stringify(data));
	  dLog('data parsed',JSON.stringify(data));
  	  dLog('template File',templateFile);

  	  var hbTemplate    = Handlebars.compile(templateFile);
	  var htmlValue = hbTemplate(data);
	  return htmlValue;
}
// end function


function preparePostData(record,miscObjects)
{
  /**
    * @desc   I obtain a template and parse it against the HandlebarsJS framework to prepare the required file for integration
    * @param  record        = the actual record being included in the integration request
    * @param  miscObjects   = typically an array of data that is to be joined to the main data object
    * @return I return the parsed file ready for being posted to the external service
    * @return I allow NS data to be accessible by data.record.<> or data.childRecords[i].name notation
  **/
  var data              = {};
      data.childRecords = [];
      data.record       = record;
      /* pass in any additional data objects to be processed */
      if(miscObjects)
      {
            data.childRecords = miscObjects;
      }
      data = JSON.parse(JSON.stringify(data));
  return data;
}
// end function






function getIntegrationTemplateFile(fileID)
{
  /**
    * @desc I obtain the file out of the file cabinet for the request
    * @param fileID       = I am the internal ID of the file being requested
    * @returns text       = I return the text file that has been found
  **/
  var file = '';
  if(isNaN(fileID))
  {

  }
  else
  {
          file = nlapiLoadFile(fileID).getValue();
  }
          return file;
}
// end function




function getIntegrationTemplate(action,record,recordSubType,integrationID)
{
          /**
            * @desc I obtain the template information for use in integration processes
            * @desc I return an object that can be used easily externally
            * @desc example        = getIntegrationTemplate(action,record,testMode);
            * @param action        = int of the action being undertaken
            * @param record        = int of the internal id of the record being processed
            * @param recordSubType = the string used to identify record subtypes. all lowercase record IDs
            * @return file         = I am the file name being returned, useful for logging purposes
            * @return internalid   = I am the internal ID of the file being returned
            * @return mode         = I am the mode being used for the processing
          **/
        var returnObject  = {};
        var results = null;
            // perform the search below for any template with the subtype defined
            if(recordSubType)
            {
                var subTypeFilters       = [];
                    subTypeFilters.push(new nlobjSearchFilter('custrecord_pm_int_template_action',null,'is',action));
                    // subTypeFilters.push(new nlobjSearchFilter('custrecord_pm_int_tem_record',null,'is',record));
                    subTypeFilters.push(new nlobjSearchFilter('custrecord_pm_int_tem_sub_type',null,'is',recordSubType));
                    if(integrationID)
                    {
                    subTypeFilters.push(new nlobjSearchFilter('custrecord_pm_int_integration',null,'is',integrationID));
                    }

                    subTypeFilters.push(new nlobjSearchFilter('custrecord_pm_int_integrate_rec',null,'is','1'));
                    subTypeFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));

                var subTypeColumns       = [];
                    subTypeColumns.push(new nlobjSearchColumn('internalid'));
                    subTypeColumns.push(new nlobjSearchColumn('custrecord_pm_int_tem_record'));
                    subTypeColumns.push(new nlobjSearchColumn('custrecord_pm_int_template_action'));
                    subTypeColumns.push(new nlobjSearchColumn('custrecord_pm_int_template'));
                    subTypeColumns.push(new nlobjSearchColumn('custrecord_pm_int_tem_sub_type'));
                    subTypeColumns.push(new nlobjSearchColumn('custrecord_pm_int_integration'));
                    subTypeColumns.push(new nlobjSearchColumn('custrecord_pm_int_integrate_rec'));         // indicates if the integration action is active
                    results       = nlapiSearchRecord('customrecord_pm_int_templates',null,subTypeFilters,subTypeColumns);                
            }
        dLog('template selection', JSON.stringify(results));
        // if no results are found, check for a base template
        if(results)
        {
          returnObject.file         = results[(results.length-1)].getText('custrecord_pm_int_template');
          returnObject.internalid   = results[(results.length-1)].getValue('custrecord_pm_int_template');
          return returnObject;
        }
}
// end function