function pistachioPageInit(type,name,form)
{
  var recType = nlapiGetRecordType();
  if(recType === 'customer' || recType === 'contact')
  {
      nlapiSetFieldValue('custentity_pm_int_update_ws_pass','F');
  }
  if(recType === 'customer')
  {
      if(nlapiGetFieldValue('isperson')  === 'F')
      {
        // set the fields as disabled 
        nlapiSetFieldDisabled('custentity_pm_int_web_password_master',true);
        nlapiSetFieldDisabled('custentity_pm_int_update_ws_pass',true);
        nlapiSetFieldValue('custentity_pm_int_sync_customer','2');
        nlapiSetFieldDisabled('custentity_pm_int_sel_webstore',true);
        nlapiSetFieldDisabled('custentity_pm_int_cust_con_id',true);
      }
  }
  // end contacts and customers 
  // item field processes
}
// end function






// control the users minimal info for the website
function pistachioFieldChanged(type,name)
{
    var recType = nlapiGetRecordType();

    if(recType === 'customer')
    {
      if(nlapiGetFieldValue('isperson') === 'F')
      {
        nlapiSetFieldDisabled('custentity_pm_int_web_password_master',true);
        nlapiSetFieldDisabled('custentity_pm_int_update_ws_pass',true);
        nlapiSetFieldDisabled('custentity_pm_int_sel_webstore',true);
		    nlapiSetFieldDisabled('custentity_pm_int_cust_con_id',true);
        if(nlapiGetFieldValue('custentity_pm_int_sync_customer') === '1')
          {
          nlapiSetFieldValue('custentity_pm_int_sync_customer','2');  
          }
        nlapiSetFieldDisabled('custentity_pm_int_sync_customer',true);
      }
      else
      {
        nlapiSetFieldDisabled('custentity_pm_int_web_password_master',false);
        nlapiSetFieldDisabled('custentity_pm_int_cust_con_id',false);
        nlapiSetFieldDisabled('custentity_pm_int_update_ws_pass',false);
        nlapiSetFieldDisabled('custentity_pm_int_sel_webstore',false);
        nlapiSetFieldDisabled('custentity_pm_int_sync_customer',false);
      }
    }
    // end customer type 

    
    if(recType === 'customer' || recType === 'contact')
    {
        if(name === 'custentity_pm_int_web_password_master')
        {
          try
            {
             var _pass = nlapiGetFieldValue('custentity_pm_int_web_password_master');
             var _strPass = nlapiSetFieldValue('custentity_pm_int_cust_pass',_pass);
            }
            catch(e)
            {}
        }
        // end password block

        if(name === 'custentity_pm_int_update_ws_pass')
        {
          if(nlapiGetFieldValue('custentity_pm_int_update_ws_pass') === 'T')
          {
              nlapiSetFieldMandatory('custentity_pm_int_web_password_master',true);
              nlapiSetFieldValue('custentity_pm_int_sync_customer','1');
          }
          else 
          {
              nlapiSetFieldMandatory('custentity_pm_int_web_password_master',false);        
              nlapiSetFieldValue('custentity_pm_int_sync_customer','2');
          }
        }
    }
    // end contact and customer process 
    var itemTypes = ['inventoryitem','assemblyitem','kititem','downloaditem','giftcertificateitem','itemgroup','lotnumberedinventoryitem','serializedinventoryitem','serviceitem','noninventoryitem','otherchargeitem'];

  if(itemTypes.indexOf(recType) >= 0)
    {
      if(nlapiGetFieldValue('custitem_pm_int_publish_item') === '1' || nlapiGetFieldValue('custitem_pm_int_publish_item') === '3')
      {
        nlapiSetFieldMandatory('custitem_pm_int_sync_to_website',true);
      }
      else
      {
        nlapiSetFieldMandatory('custitem_pm_int_sync_to_website',false);        
      }      
    }
}
// end function


function pistachioSaveRecord(type,name)
{
  var recType = nlapiGetRecordType();
  if(recType === 'customer' || recType === 'contact')
  {
      var updatePassword = nlapiGetFieldValue('custentity_pm_int_update_ws_pass');
      if(updatePassword === 'T' && nlapiGetFieldValue('custentity_pm_int_web_password_master') === '')
      {
        alert('Please enter a website password for this entity');
        return false;
      }
  }
  
  if(nlapiGetFieldValue('custentity_pm_int_sync_customer') === '1')
  {
    if(nlapiGetFieldValue('email') === '')
    {
      alert('Please provide an email address for this user so they may access your website');
      return false;
    }
    if(nlapiGetFieldValue('custentity_pm_int_sel_webstore') === '')
    {
      alert('Please select a website for this customer to be granted access to');
      return false;
    }
  return true;
  }
  // end conact and customer record processes 
  var itemTypes = ['inventoryitem','assemblyitem','kititem','downloaditem','giftcertificateitem','itemgroup','lotnumberedinventoryitem','serializedinventoryitem','serviceitem','noninventoryitem','otherchargeitem'];

if(itemTypes.indexOf(recType) >= 0)      
//  if(recType === 'inventoryitem')
  {
    if(nlapiGetFieldValue('custitem_pm_int_publish_item') === '1')
    {
      if(nlapiGetFieldValue('custitem_pm_int_sync_to_website') === '')
      {
        alert('Please select a website for this item to be assigned to');
        return false;
      }
    }
    else
    {
        return true;
    }
    return true;
  }


return true;
}
// end function 